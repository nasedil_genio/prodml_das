# License notice
#
# Copyright 2017
#
# Energistics
# The following Energistics (c) products were used in the creation of this work:
#
# •             PRODML Data Schema Specifications, Version 1.3.1
# •             PRODML Data Schema Specifications, Version 2.0
#
# All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
# any portion thereof, which remain in the Standards DevKit shall remain with Energistics
# or its suppliers and shall remain subject to the terms of the Product License Agreement
# available at http://www.energistics.org/product-license-agreement.
#
# Apache
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
# except in compliance with the License.
#
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the
# License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the License for the specific language governing permissions and limitations under the
# License.
#
# All rights reserved.
#

import datetime as dt
import os

import aniso8601
import h5py
import numpy as np
import pytest

from prodml_das.prodml import PMLproxy
from prodml_das.prodml import VALIDATION_LEVEL_EXTERNAL_CONTENT

def test_testcase_1():
    verify_testcase_1(os.path.join(os.path.dirname(__file__), '../data/Case1/Case1.epc'))

def verify_testcase_1(filename):
    """Verify content of test case from standard examples."""
    pml = PMLproxy(filename, validation_level=VALIDATION_LEVEL_EXTERNAL_CONTENT)
    verify_das_acquisition(pml)
    verify_fiber_optical_path(pml)
    verify_das_instrument_box(pml)
    verify_hdf(pml)

def verify_das_acquisition(pml):
    """Verify DasAcquisition contents."""
    assert pml.das_acquisition
    a = pml.das_acquisition # shorthand
    ## Aliases.
    assert len(a.Aliases) == 1
    assert a.Aliases[0].authority == 'abc'
    assert a.Aliases[0].Description == None
    assert a.Aliases[0].Identifier == 'My Alias'
    ## Citation.
    assert a.Citation.Creation == aniso8601.parse_datetime('2015-07-20T01:00:00+01:00')
    assert a.Citation.Description == None
    assert a.Citation.DescriptiveKeywords == None
    assert a.Citation.Editor == None
    assert a.Citation.Format == 'Energistics'
    assert a.Citation.LastUpdate == None
    assert a.Citation.Originator == 'Energistics'
    assert a.Citation.Title == 'DAS Acquisition'
    assert a.Citation.VersionString == None
    ## CustomData.
    assert a.CustomData
    ## existenceKind.
    assert a.existenceKind == None
    ## ExtensionNameValue.
    assert len(a.ExtensionNameValue) == 1
    assert a.ExtensionNameValue[0].Description == None
    assert a.ExtensionNameValue[0].DTim == None
    assert a.ExtensionNameValue[0].Index == None
    assert a.ExtensionNameValue[0].MeasureClass == None
    assert a.ExtensionNameValue[0].Name == 'customInt'
    assert a.ExtensionNameValue[0].Value.valueOf_ == '2'
    assert a.ExtensionNameValue[0].Value.uom == None
    ## objectVersion.
    assert a.objectVersion == None
    ## schemaVersion.
    assert a.schemaVersion == '2.0'
    ## uuid.
    assert a.uuid == '8d97d49b-4a49-4ffd-908b-7e49b0c0f25b'
    ## AcquisitionDescription.
    assert a.AcquisitionDescription == 'Energistics DAS PRODML Acquisition Sample'
    ## AcquisitionId.
    assert a.AcquisitionId == 'dc0e381a-094a-4fd2-ab89-dce867e3b99d'
    ## DasInstrumentBox.
    assert a.DasInstrumentBox.ContentType == 'application/x-prodml+xml;version=2.0;type=DasInstrumentBox'
    assert a.DasInstrumentBox.Title == 'Instrument Box'
    assert a.DasInstrumentBox.Uuid == 'df9447a1-8c6b-4634-88eb-ab07b41ac876'
    ## FacilityId.
    assert len(a.FacilityId) == 2
    assert a.FacilityId[0] == 'ABC Facility'
    assert a.FacilityId[1] == 'Well Facility'
    ## GaugeLength.
    assert float(a.GaugeLength.valueOf_) == 40.0
    assert a.GaugeLength.uom == 'm'
    ## GaugeLengthUnit.
    assert a.GaugeLengthUnit == 'm'
    ## MaximumFrequency.
    assert float(a.MaximumFrequency.valueOf_) == 25.0
    assert a.MaximumFrequency.uom == 'Hz'
    ## MeasurementStartTime.
    assert a.MeasurementStartTime == '2015-07-20T01:23:45.123456+01:00'
    ## MinimumFrequency.
    assert float(a.MinimumFrequency.valueOf_) == 0.5
    assert a.MinimumFrequency.uom == 'Hz'
    ## NumberOfLoci.
    assert a.NumberOfLoci == 5
    ## OpticalPath.
    assert a.OpticalPath.ContentType == 'application/x-prodml+xml;version=2.0;type=FiberOpticalPath'
    assert a.OpticalPath.Title == 'OptPath1'
    assert a.OpticalPath.Uuid == 'bfd164f4-f9e3-41c0-a74e-372b69cc2a09'
    ## PulseRate.
    assert float(a.PulseRate.valueOf_) == 50.0
    assert a.PulseRate.uom == 'Hz'
    ## PulseWidth.
    assert float(a.PulseWidth.valueOf_) == 8.0
    assert a.PulseWidth.uom == 'ns'
    ## PulseWidthUnit.
    assert a.PulseWidthUnit == 'ns'
    ## SpatialSamplingInterval.
    assert float(a.SpatialSamplingInterval.valueOf_) == 5.0
    assert a.SpatialSamplingInterval.uom == 'm'
    ## SpatialSamplingIntervalUnit.
    assert a.SpatialSamplingIntervalUnit == 'm'
    ## StartLocusIndex.
    assert a.StartLocusIndex == 0
    ## TriggeredMeasurement.
    assert a.TriggeredMeasurement == False
    ## VendorCode.
    assert a.VendorCode.Address.City == 'Austin'
    assert a.VendorCode.Address.Country == 'USA'
    assert a.VendorCode.Address.County == 'Texas'
    assert a.VendorCode.Address.kind == None
    assert a.VendorCode.Address.Name == 'Wyle E Coyote'
    assert a.VendorCode.Address.PostalCode == '78701'
    assert a.VendorCode.Address.Province == 'Texas'
    assert a.VendorCode.Address.State == 'Texas'
    assert len(a.VendorCode.Address.Street) == 2
    assert a.VendorCode.Address.Street[0] == 'Suite 2100'
    assert a.VendorCode.Address.Street[1] == '515 Congress Avenue'
    assert a.VendorCode.Address.uid == 'main'
    assert len(a.VendorCode.Alias) == 1
    assert a.VendorCode.Alias[0].valueOf_ == ''
    assert a.VendorCode.Alias[0].authority == None
    assert a.VendorCode.AssociatedWith == ''
    assert a.VendorCode.Contact == ['']
    assert len(a.VendorCode.Email) == 1
    # TODO add email in the example and verify it (currently not used because of no consensus regarding email type in xsd)
    assert a.VendorCode.Email[0].qualifier == None
    assert a.VendorCode.Name == 'ABCDE'
    assert a.VendorCode.PersonnelCount == None
    assert len(a.VendorCode.PhoneNumber) == 1
    assert a.VendorCode.PhoneNumber[0].valueOf_ == '555-555-5555'
    assert a.VendorCode.PhoneNumber[0].extension == None
    assert a.VendorCode.PhoneNumber[0].qualifier == None
    assert a.VendorCode.PhoneNumber[0].type_ == 'voice'
    assert len(a.VendorCode.Role) == 2
    assert a.VendorCode.Role[0].valueOf_ == 'operator'
    assert a.VendorCode.Role[0].authority == None
    assert a.VendorCode.Role[1].valueOf_ == 'owner'
    assert a.VendorCode.Role[1].authority == None
    assert a.VendorCode.PersonName.First == 'Wyle'
    assert a.VendorCode.PersonName.Middle == 'E'
    assert a.VendorCode.PersonName.Last == 'Coyote'
    assert a.VendorCode.PersonName.Prefix == None
    assert a.VendorCode.PersonName.Suffix == ['Esq.', 'PhD.']
    ## Calibration.
    assert len(a.Calibration) == 2
    assert a.Calibration[0].CalibrationDatum == None
    assert a.Calibration[0].CalibrationDescription == None
    assert a.Calibration[0].CalibrationFacilityLengthUnit == None
    assert a.Calibration[0].CalibrationIndex == None
    assert a.Calibration[0].CalibrationOpticalPathDistanceUnit == None
    assert a.Calibration[0].FacilityKind == 'generic'
    assert a.Calibration[0].FacilityName == 'Facility name'
    assert a.Calibration[0].NumberOfCalibrationPoints == 9
    assert len(a.Calibration[0].CalibrationDataPoints) == 9
    assert float(a.Calibration[0].CalibrationDataPoints[0].CalibrationFacilityLength.valueOf_) == 23.5
    assert a.Calibration[0].CalibrationDataPoints[0].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[0].CalibrationLocusIndex == 4
    assert float(a.Calibration[0].CalibrationDataPoints[0].CalibrationOpticalPathDistance.valueOf_) == 23.5
    assert a.Calibration[0].CalibrationDataPoints[0].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[0].CalibrationType == 'tap test'
    assert float(a.Calibration[0].CalibrationDataPoints[1].CalibrationFacilityLength.valueOf_) == 12.43
    assert a.Calibration[0].CalibrationDataPoints[1].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[1].CalibrationLocusIndex == 101
    assert float(a.Calibration[0].CalibrationDataPoints[1].CalibrationOpticalPathDistance.valueOf_) == -1.0
    assert a.Calibration[0].CalibrationDataPoints[1].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[1].CalibrationType == 'last locus to end of fiber'
    assert float(a.Calibration[0].CalibrationDataPoints[2].CalibrationFacilityLength.valueOf_) == 5.0
    assert a.Calibration[0].CalibrationDataPoints[2].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[2].CalibrationLocusIndex == 0
    assert float(a.Calibration[0].CalibrationDataPoints[2].CalibrationOpticalPathDistance.valueOf_) == 5.0
    assert a.Calibration[0].CalibrationDataPoints[2].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[2].CalibrationType == 'locus calibration'
    assert float(a.Calibration[0].CalibrationDataPoints[3].CalibrationFacilityLength.valueOf_) == 10.0
    assert a.Calibration[0].CalibrationDataPoints[3].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[3].CalibrationLocusIndex == 1
    assert float(a.Calibration[0].CalibrationDataPoints[3].CalibrationOpticalPathDistance.valueOf_) == 10.0
    assert a.Calibration[0].CalibrationDataPoints[3].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[3].CalibrationType == 'locus calibration'
    assert float(a.Calibration[0].CalibrationDataPoints[4].CalibrationFacilityLength.valueOf_) == 25.0
    assert a.Calibration[0].CalibrationDataPoints[4].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[4].CalibrationLocusIndex == 4
    assert float(a.Calibration[0].CalibrationDataPoints[4].CalibrationOpticalPathDistance.valueOf_) == 25.0
    assert a.Calibration[0].CalibrationDataPoints[4].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[4].CalibrationType == 'locus calibration'
    assert float(a.Calibration[0].CalibrationDataPoints[5].CalibrationFacilityLength.valueOf_) == 29.907
    assert a.Calibration[0].CalibrationDataPoints[5].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[5].CalibrationLocusIndex == 5
    assert float(a.Calibration[0].CalibrationDataPoints[5].CalibrationOpticalPathDistance.valueOf_) == 30.0
    assert a.Calibration[0].CalibrationDataPoints[5].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[5].CalibrationType == 'locus calibration'
    assert float(a.Calibration[0].CalibrationDataPoints[6].CalibrationFacilityLength.valueOf_) == 34.814
    assert a.Calibration[0].CalibrationDataPoints[6].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[6].CalibrationLocusIndex == 6
    assert float(a.Calibration[0].CalibrationDataPoints[6].CalibrationOpticalPathDistance.valueOf_) == 35.0
    assert a.Calibration[0].CalibrationDataPoints[6].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[6].CalibrationType == 'locus calibration'
    assert float(a.Calibration[0].CalibrationDataPoints[7].CalibrationFacilityLength.valueOf_) == 491.143
    assert a.Calibration[0].CalibrationDataPoints[7].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[7].CalibrationLocusIndex == 99
    assert float(a.Calibration[0].CalibrationDataPoints[7].CalibrationOpticalPathDistance.valueOf_) == 500.0
    assert a.Calibration[0].CalibrationDataPoints[7].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[7].CalibrationType == 'locus calibration'
    assert float(a.Calibration[0].CalibrationDataPoints[8].CalibrationFacilityLength.valueOf_) == 496.05
    assert a.Calibration[0].CalibrationDataPoints[8].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[8].CalibrationLocusIndex == 100
    assert float(a.Calibration[0].CalibrationDataPoints[8].CalibrationOpticalPathDistance.valueOf_) == 505.0
    assert a.Calibration[0].CalibrationDataPoints[8].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[0].CalibrationDataPoints[8].CalibrationType == 'locus calibration'
    assert a.Calibration[1].CalibrationDatum == None
    assert a.Calibration[1].CalibrationDescription == None
    assert a.Calibration[1].CalibrationFacilityLengthUnit == None
    assert a.Calibration[1].CalibrationIndex == None
    assert a.Calibration[1].CalibrationOpticalPathDistanceUnit == None
    assert a.Calibration[1].FacilityKind == 'generic'
    assert a.Calibration[1].FacilityName == 'Facility name'
    assert a.Calibration[1].NumberOfCalibrationPoints == 5
    assert len(a.Calibration[1].CalibrationDataPoints) == 5
    assert float(a.Calibration[1].CalibrationDataPoints[0].CalibrationFacilityLength.valueOf_) == 4.907
    assert a.Calibration[1].CalibrationDataPoints[0].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[1].CalibrationDataPoints[0].CalibrationLocusIndex == 5
    assert float(a.Calibration[1].CalibrationDataPoints[0].CalibrationOpticalPathDistance.valueOf_) == 30.0
    assert a.Calibration[1].CalibrationDataPoints[0].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[1].CalibrationDataPoints[0].CalibrationType == 'locus calibration'
    assert float(a.Calibration[1].CalibrationDataPoints[1].CalibrationFacilityLength.valueOf_) == 9.814
    assert a.Calibration[1].CalibrationDataPoints[1].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[1].CalibrationDataPoints[1].CalibrationLocusIndex == 6
    assert float(a.Calibration[1].CalibrationDataPoints[1].CalibrationOpticalPathDistance.valueOf_) == 35.0
    assert a.Calibration[1].CalibrationDataPoints[1].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[1].CalibrationDataPoints[1].CalibrationType == 'locus calibration'
    assert float(a.Calibration[1].CalibrationDataPoints[2].CalibrationFacilityLength.valueOf_) == 466.143
    assert a.Calibration[1].CalibrationDataPoints[2].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[1].CalibrationDataPoints[2].CalibrationLocusIndex == 99
    assert float(a.Calibration[1].CalibrationDataPoints[2].CalibrationOpticalPathDistance.valueOf_) == 500.0
    assert a.Calibration[1].CalibrationDataPoints[2].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[1].CalibrationDataPoints[2].CalibrationType == 'locus calibration'
    assert float(a.Calibration[1].CalibrationDataPoints[3].CalibrationFacilityLength.valueOf_) == 471.05
    assert a.Calibration[1].CalibrationDataPoints[3].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[1].CalibrationDataPoints[3].CalibrationLocusIndex == 100
    assert float(a.Calibration[1].CalibrationDataPoints[3].CalibrationOpticalPathDistance.valueOf_) == 505.0
    assert a.Calibration[1].CalibrationDataPoints[3].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[1].CalibrationDataPoints[3].CalibrationType == 'locus calibration'
    assert float(a.Calibration[1].CalibrationDataPoints[4].CalibrationFacilityLength.valueOf_) == -1.0
    assert a.Calibration[1].CalibrationDataPoints[4].CalibrationFacilityLength.uom == 'm'
    assert a.Calibration[1].CalibrationDataPoints[4].CalibrationLocusIndex == 100
    assert float(a.Calibration[1].CalibrationDataPoints[4].CalibrationOpticalPathDistance.valueOf_) == 12.43
    assert a.Calibration[1].CalibrationDataPoints[4].CalibrationOpticalPathDistance.uom == 'm'
    assert a.Calibration[1].CalibrationDataPoints[4].CalibrationType == 'last locus to end of fiber'
    ## Custom.
    assert a.Custom
    ## Processed.
    assert a.Processed.Fbe == []
    assert a.Processed.Spectra == []
    ## Raw.
    assert len(a.Raw) == 1
    assert a.Raw[0].NumberOfLoci == 5
    assert float(a.Raw[0].OutputDataRate.valueOf_) == 50.0
    assert a.Raw[0].OutputDataRate.uom == 'Hz'
    assert a.Raw[0].RawData.Dimensions == ['time', 'locus']
    assert a.Raw[0].RawData.RawDataArray.extensiontype_ == 'eml:DoubleExternalArray'
    assert len(a.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy) == 1
    assert a.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].extensiontype_ == 'prodml:DasExternalDatasetPart'
    assert a.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].Count == 5000
    assert a.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].PathInExternalFile == '/Acquisition/Raw[0]/RawData'
    assert a.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].StartIndex == 0
    assert a.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].EpcExternalPartReference.ContentType == 'application/x-eml+xml;version=2.1;type=EpcExternalPartReference'
    assert a.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].EpcExternalPartReference.Title == 'Hdf Proxy'
    assert a.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].EpcExternalPartReference.Uuid == '3810705d-1b4f-4041-8c2e-7716e1a47964'
    assert a.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].PartEndTime == '2015-07-20T01:24:05.658000+01:00'
    assert a.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].PartStartTime == '2015-07-20T01:23:45.678000+01:00'
    assert a.Raw[0].RawDataTime.EndTime == '2015-07-20T01:24:05.658000+01:00'
    assert a.Raw[0].RawDataTime.StartTime == '2015-07-20T01:23:45.678000+01:00'
    assert a.Raw[0].RawDataTime.TimeArray.NullValue == -1
    assert a.Raw[0].RawDataTime.TimeArray.extensiontype_ == 'eml:IntegerExternalArray'
    assert len(a.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy) == 1
    assert a.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].extensiontype_ == 'prodml:DasExternalDatasetPart'
    assert a.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].Count == 1000
    assert a.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].PathInExternalFile == '/Acquisition/Raw[0]/RawDataTime'
    assert a.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].StartIndex == 0
    assert a.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].EpcExternalPartReference.ContentType == 'application/x-eml+xml;version=2.1;type=EpcExternalPartReference'
    assert a.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].EpcExternalPartReference.Title == 'Hdf Proxy'
    assert a.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].EpcExternalPartReference.Uuid == '3810705d-1b4f-4041-8c2e-7716e1a47964'
    assert a.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].PartEndTime == '2015-07-20T01:24:05.658000+01:00'
    assert a.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].PartStartTime == '2015-07-20T01:23:45.678000+01:00'
    assert a.Raw[0].RawDataTriggerTime.EndTime == '2015-07-20T01:24:05.658000+01:00'
    assert a.Raw[0].RawDataTriggerTime.StartTime == '2015-07-20T01:23:45.678000+01:00'
    assert a.Raw[0].RawDataTriggerTime.TimeArray.NullValue == -1
    assert a.Raw[0].RawDataTriggerTime.TimeArray.extensiontype_ == 'eml:IntegerExternalArray'
    assert len(a.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy) == 1
    assert a.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0].extensiontype_ == 'prodml:DasExternalDatasetPart'
    assert a.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0].Count == 1
    assert a.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0].PathInExternalFile == '/Acquisition/Raw[0]/RawDataTriggerTime'
    assert a.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0].StartIndex == 0
    assert a.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0].EpcExternalPartReference.ContentType == 'application/x-eml+xml;version=2.1;type=EpcExternalPartReference'
    assert a.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0].EpcExternalPartReference.Title == 'Hdf Proxy'
    assert a.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0].EpcExternalPartReference.Uuid == '3810705d-1b4f-4041-8c2e-7716e1a47964'
    assert a.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0].PartEndTime == '2015-07-20T01:23:45.678000+01:00'
    assert a.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0].PartStartTime == '2015-07-20T01:23:45.678000+01:00'
    assert a.Raw[0].RawDataUnit == 'V'
    assert a.Raw[0].RawDescription == None
    assert a.Raw[0].RawIndex == None
    assert a.Raw[0].StartLocusIndex == 0
    assert a.Raw[0].uuid == 'c1dedc3c-412c-40ae-8ffc-88359ef0da9d'
    assert a.Raw[0].Custom

def verify_fiber_optical_path(pml):
    """Verify FiberOpticalPath contents."""
    assert pml.fiber_optical_path
    f = pml.fiber_optical_path # shorthand
    ## Aliases.
    assert len(f.Aliases) == 0
    ## Citation.
    assert f.Citation.Creation == aniso8601.parse_datetime('2005-07-20T01:00:00')
    assert f.Citation.Description == 'FiberOpticalPath DAS worked example'
    assert f.Citation.DescriptiveKeywords == None
    assert f.Citation.Editor == None
    assert f.Citation.Format == '1'
    assert f.Citation.LastUpdate == None
    assert f.Citation.Originator == 'Source'
    assert f.Citation.Title == 'OptPath1'
    assert f.Citation.VersionString == None
    ## CustomData.
    assert f.CustomData == None
    ## existenceKind.
    assert f.existenceKind == None
    ## ExtensionNameValue.
    assert len(f.ExtensionNameValue) == 0
    ## objectVersion.
    assert f.objectVersion == None
    ## schemaVersion.
    assert f.schemaVersion == '2.0'
    ## uuid.
    assert f.uuid == 'bfd164f4-f9e3-41c0-a74e-372b69cc2a09'
    ## Defect.
    assert len(f.Defect) == 1
    assert f.Defect[0].Comment == 'Consistent signal spike detected at this location'
    assert f.Defect[0].defectID == 'OPTDEFECT1'
    assert f.Defect[0].DefectType == 'other'
    assert f.Defect[0].OpticalPathDistanceEnd == None
    assert float(f.Defect[0].OpticalPathDistanceStart.valueOf_) == 352.00
    assert f.Defect[0].OpticalPathDistanceStart.uom == 'ft'
    assert f.Defect[0].TimeEnd == None
    assert f.Defect[0].TimeStart == aniso8601.parse_datetime('2005-07-20T01:00:00')
    ## FacilityIdentifier.
    assert f.FacilityIdentifier.ContextFacility == None
    assert f.FacilityIdentifier.Installation == None
    assert f.FacilityIdentifier.Kind == None
    assert f.FacilityIdentifier.Name.valueOf_ == 'Well-01'
    assert f.FacilityIdentifier.Name.authority == None
    assert f.FacilityIdentifier.uid == ''
    assert f.FacilityIdentifier.BusinessUnit == None
    assert f.FacilityIdentifier.GeographicContext == None
    assert f.FacilityIdentifier.Operator == None
    ## FacilityMapping.
    assert len(f.FacilityMapping) == 1
    assert f.FacilityMapping[0].Comment == None
    assert f.FacilityMapping[0].TimeEnd == None
    assert f.FacilityMapping[0].TimeStart == aniso8601.parse_datetime('2005-07-20T01:00:00')
    assert f.FacilityMapping[0].uid == 'FM1'
    assert len(f.FacilityMapping[0].FiberFacilityMappingPart) == 2
    assert f.FacilityMapping[0].FiberFacilityMappingPart[0].Comment == "No 'timeEnd' specified since this mapping is still valid. No overstuffing in surface cable"
    assert float(f.FacilityMapping[0].FiberFacilityMappingPart[0].FacilityLengthEnd.valueOf_) == 25.0
    assert f.FacilityMapping[0].FiberFacilityMappingPart[0].FacilityLengthEnd.uom == 'm'
    assert float(f.FacilityMapping[0].FiberFacilityMappingPart[0].FacilityLengthStart.valueOf_) == 0.0
    assert f.FacilityMapping[0].FiberFacilityMappingPart[0].FacilityLengthStart.uom == 'm'
    assert float(f.FacilityMapping[0].FiberFacilityMappingPart[0].OpticalPathDistanceEnd.valueOf_) == 25.0
    assert f.FacilityMapping[0].FiberFacilityMappingPart[0].OpticalPathDistanceEnd.uom == 'm'
    assert float(f.FacilityMapping[0].FiberFacilityMappingPart[0].OpticalPathDistanceStart.valueOf_) == 0.0
    assert f.FacilityMapping[0].FiberFacilityMappingPart[0].OpticalPathDistanceStart.uom == 'm'
    assert f.FacilityMapping[0].FiberFacilityMappingPart[0].uid == 'FMP1'
    assert f.FacilityMapping[0].FiberFacilityMappingPart[0].FiberFacility.extensiontype_ == 'prodml:FiberFacilityGeneric'
    assert f.FacilityMapping[0].FiberFacilityMappingPart[0].FiberFacility.FacilityKind == 'Surface Cable'
    assert f.FacilityMapping[0].FiberFacilityMappingPart[0].FiberFacility.FacilityName == 'Surface Cable 1'
    assert f.FacilityMapping[0].FiberFacilityMappingPart[1].Comment == "No 'timeEnd' specified since this mapping is still valid. The over stuffing of 9.182 m equally distributed along the installed downhole cable"
    assert float(f.FacilityMapping[0].FiberFacilityMappingPart[1].FacilityLengthEnd.valueOf_) == 483.25
    assert f.FacilityMapping[0].FiberFacilityMappingPart[1].FacilityLengthEnd.uom == 'm'
    assert float(f.FacilityMapping[0].FiberFacilityMappingPart[1].FacilityLengthStart.valueOf_) == 0.0
    assert f.FacilityMapping[0].FiberFacilityMappingPart[1].FacilityLengthStart.uom == 'm'
    assert float(f.FacilityMapping[0].FiberFacilityMappingPart[1].OpticalPathDistanceEnd.valueOf_) == 517.43
    assert f.FacilityMapping[0].FiberFacilityMappingPart[1].OpticalPathDistanceEnd.uom == 'm'
    assert float(f.FacilityMapping[0].FiberFacilityMappingPart[1].OpticalPathDistanceStart.valueOf_) == 25.0
    assert f.FacilityMapping[0].FiberFacilityMappingPart[1].OpticalPathDistanceStart.uom == 'm'
    assert f.FacilityMapping[0].FiberFacilityMappingPart[1].uid == 'FMP2'
    assert f.FacilityMapping[0].FiberFacilityMappingPart[1].FiberFacility.extensiontype_ == 'prodml:FiberFacilityWell'
    assert f.FacilityMapping[0].FiberFacilityMappingPart[1].FiberFacility.Name == 'Well-01 Fiber'
    assert f.FacilityMapping[0].FiberFacilityMappingPart[1].FiberFacility.WellDatum == 'kelly bushing'
    assert f.FacilityMapping[0].FiberFacilityMappingPart[1].FiberFacility.WellboreReference.ContentType == 'application/x-witsml+xml;version=2.0;type=wellbore'
    assert f.FacilityMapping[0].FiberFacilityMappingPart[1].FiberFacility.WellboreReference.Title == 'Main wellbore of well Well-01'
    assert f.FacilityMapping[0].FiberFacilityMappingPart[1].FiberFacility.WellboreReference.Uuid == '4ab0fae6-dc0e-405a-b812-203a154c1243'
    ## OpticalPathNetwork
    assert len(f.OpticalPathNetwork) == 1
    assert f.OpticalPathNetwork[0].Comment == None
    assert len(f.OpticalPathNetwork[0].ContextFacility) == 1
    assert f.OpticalPathNetwork[0].ContextFacility[0].kind == None
    assert f.OpticalPathNetwork[0].ContextFacility[0].namingSystem == None
    assert f.OpticalPathNetwork[0].ContextFacility[0].siteKind == None
    assert f.OpticalPathNetwork[0].ContextFacility[0].uidRef == None
    assert f.OpticalPathNetwork[0].ContextFacility[0].valueOf_ == 'text'
    assert f.OpticalPathNetwork[0].DTimeEnd == None
    assert f.OpticalPathNetwork[0].DTimMax == None
    assert f.OpticalPathNetwork[0].DTimMin == None
    assert f.OpticalPathNetwork[0].DTimStart == None
    assert f.OpticalPathNetwork[0].ExistenceTime == None
    assert f.OpticalPathNetwork[0].Installation == None
    assert f.OpticalPathNetwork[0].uid == 'OPN1'
    assert f.OpticalPathNetwork[0].ExternalConnect == []
    assert len(f.OpticalPathNetwork[0].Network) == 1
    assert f.OpticalPathNetwork[0].Network[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Name == 'Current Setup Well-01'
    assert f.OpticalPathNetwork[0].Network[0].ParentNetworkReference == None
    assert f.OpticalPathNetwork[0].Network[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].uid == 'N1'
    assert f.OpticalPathNetwork[0].Network[0].Plan == []
    assert len(f.OpticalPathNetwork[0].Network[0].Unit) == 4
    ### Unit 0
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].ContextFacility == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Facility.kind == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Facility.namingSystem == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Facility.siteKind == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Facility.uidRef == '10'
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Facility.valueOf_ == 'Surface Fiber Segment'
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].FacilityParent1 == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].FacilityParent2 == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].InternalNetworkReference == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Name == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].uid == 'OPNU10'
    assert len(f.OpticalPathNetwork[0].Network[0].Unit[0].Port) == 2
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].Direction == 'inlet'
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].Exposed == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].Facility == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].FacilityAlias == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].Name == 'Connection to LightBox'
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].uid == '1'
    assert len(f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].ConnectedNode) == 1
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].ConnectedNode[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].ConnectedNode[0].DTimEnd == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].ConnectedNode[0].DTimStart == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].ConnectedNode[0].Node == 'Instrument Box'
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].ConnectedNode[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].ConnectedNode[0].uid == 'CN'
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].ExpectedFlowProperty == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[0].ExpectedFlowProduct == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].Direction == 'outlet'
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].Exposed == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].Facility == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].FacilityAlias == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].Name == 'Connection to Surface Connector'
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].uid == '2'
    assert len(f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].ConnectedNode) == 1
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].ConnectedNode[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].ConnectedNode[0].DTimEnd == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].ConnectedNode[0].DTimStart == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].ConnectedNode[0].Node == 'Surface Connector 1'
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].ConnectedNode[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].ConnectedNode[0].uid == 'CSC1'
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].ExpectedFlowProperty == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].Port[1].ExpectedFlowProduct == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].ExpectedProperty == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].FacilityAlias == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[0].RelativeCoordinate == None
    ### Unit 1
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].ContextFacility == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Facility.kind == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Facility.namingSystem == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Facility.siteKind == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Facility.uidRef == '20'
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Facility.valueOf_ == 'Surface Connector'
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].FacilityParent1 == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].FacilityParent2 == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].InternalNetworkReference == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Name == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].uid == 'OPNU20'
    assert len(f.OpticalPathNetwork[0].Network[0].Unit[1].Port) == 2
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].Direction == 'inlet'
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].Exposed == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].Facility == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].FacilityAlias == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].Name == 'Connection from Surface Connector to Surface Fiber'
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].uid == '3'
    assert len(f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].ConnectedNode) == 1
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].ConnectedNode[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].ConnectedNode[0].DTimEnd == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].ConnectedNode[0].DTimStart == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].ConnectedNode[0].Node == 'Surface Connector 1'
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].ConnectedNode[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].ConnectedNode[0].uid == 'CSC1'
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].ExpectedFlowProperty == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[0].ExpectedFlowProduct == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].Direction == 'outlet'
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].Exposed == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].Facility == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].FacilityAlias == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].Name == 'Connection from Surface Connector to Downhole Fiber'
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].uid == '4'
    assert len(f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].ConnectedNode) == 1
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].ConnectedNode[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].ConnectedNode[0].DTimEnd == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].ConnectedNode[0].DTimStart == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].ConnectedNode[0].Node == 'Surface Connector 2'
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].ConnectedNode[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].ConnectedNode[0].uid == 'CDF1'
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].ExpectedFlowProperty == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].Port[1].ExpectedFlowProduct == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].ExpectedProperty == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].FacilityAlias == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[1].RelativeCoordinate == None
    ### Unit 0
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].ContextFacility == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Facility.kind == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Facility.namingSystem == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Facility.siteKind == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Facility.uidRef == '30'
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Facility.valueOf_ == 'Downhole Fiber'
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].FacilityParent1 == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].FacilityParent2 == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].InternalNetworkReference == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Name == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].uid == 'OPNU30'
    assert len(f.OpticalPathNetwork[0].Network[0].Unit[2].Port) == 2
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].Direction == 'inlet'
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].Exposed == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].Facility == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].FacilityAlias == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].Name == 'Connection from Surface Connector to Downhole Fiber'
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].uid == '5'
    assert len(f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].ConnectedNode) == 1
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].ConnectedNode[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].ConnectedNode[0].DTimEnd == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].ConnectedNode[0].DTimStart == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].ConnectedNode[0].Node == 'Surface Connector 2'
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].ConnectedNode[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].ConnectedNode[0].uid == 'CDF1'
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].ExpectedFlowProperty == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[0].ExpectedFlowProduct == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].Direction == 'outlet'
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].Exposed == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].Facility == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].FacilityAlias == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].Name == 'Connection from Downhole Fiber to Terminator'
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].uid == '6'
    assert len(f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].ConnectedNode) == 1
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].ConnectedNode[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].ConnectedNode[0].DTimEnd == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].ConnectedNode[0].DTimStart == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].ConnectedNode[0].Node == 'Downhole Connection 1'
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].ConnectedNode[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].ConnectedNode[0].uid == 'CT1'
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].ExpectedFlowProperty == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].Port[1].ExpectedFlowProduct == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].ExpectedProperty == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].FacilityAlias == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[2].RelativeCoordinate == None
    ### Unit 3
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].ContextFacility == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Facility.kind == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Facility.namingSystem == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Facility.siteKind == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Facility.uidRef == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Facility.valueOf_ == 'Terminator'
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].FacilityParent1 == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].FacilityParent2 == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].InternalNetworkReference == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Name == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].uid == 'OPNU40'
    assert len(f.OpticalPathNetwork[0].Network[0].Unit[3].Port) == 1
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].Direction == 'inlet'
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].Exposed == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].Facility == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].FacilityAlias == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].Name == 'Connection from Downhole Fiber to Terminator'
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].uid == '7'
    assert len(f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].ConnectedNode) == 1
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].ConnectedNode[0].Comment == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].ConnectedNode[0].DTimEnd == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].ConnectedNode[0].DTimStart == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].ConnectedNode[0].Node == 'Downhole Connection 1'
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].ConnectedNode[0].PlanName == None
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].ConnectedNode[0].uid == 'CT1'
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].ExpectedFlowProperty == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].Port[0].ExpectedFlowProduct == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].ExpectedProperty == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].FacilityAlias == []
    assert f.OpticalPathNetwork[0].Network[0].Unit[3].RelativeCoordinate == None
    assert f.OpticalPathNetwork[0].Network[0].ChangeLog == []
    assert f.OpticalPathNetwork[0].Network[0].Port == []
    ## InstallingVendor.
    assert f.InstallingVendor.Address == None
    assert len(f.InstallingVendor.Alias) == 0
    assert f.InstallingVendor.AssociatedWith == None
    assert f.InstallingVendor.Contact == ['Mr A Vendor']
    assert len(f.InstallingVendor.Email) == 0
    assert f.InstallingVendor.Name == 'Vendor Name'
    assert f.InstallingVendor.PersonnelCount == None
    assert len(f.InstallingVendor.PhoneNumber) == 0
    assert len(f.InstallingVendor.Role) == 0
    assert f.InstallingVendor.PersonName == None
    ## Inventory.
    assert len(f.Inventory.Connection) == 1
    assert f.Inventory.Connection[0].Comment == None
    assert f.Inventory.Connection[0].Manufacturer == None
    assert f.Inventory.Connection[0].ManufacturingDate == None
    assert f.Inventory.Connection[0].Name == 'Surface Connector'
    assert f.Inventory.Connection[0].SoftwareVersion == None
    assert f.Inventory.Connection[0].SupplierModelNumber == None
    assert f.Inventory.Connection[0].SupplyDate == None
    assert f.Inventory.Connection[0].Type == 'connector'
    assert f.Inventory.Connection[0].Loss == None
    assert f.Inventory.Connection[0].ReasonForDecommissioning == None
    assert f.Inventory.Connection[0].Reflectance == None
    assert f.Inventory.Connection[0].uid == '20'
    assert f.Inventory.Connection[0].ConnectorType == 'dry mate'
    assert f.Inventory.Connection[0].EndType == None
    assert len(f.Inventory.Segment) == 2
    assert f.Inventory.Segment[0].Manufacturer == None
    assert f.Inventory.Segment[0].ManufacturingDate == None
    assert f.Inventory.Segment[0].Name == 'Surface Fiber'
    assert f.Inventory.Segment[0].SoftwareVersion == None
    assert f.Inventory.Segment[0].SupplierModelNumber == None
    assert f.Inventory.Segment[0].SupplyDate == None
    assert f.Inventory.Segment[0].Type == 'fiber'
    assert f.Inventory.Segment[0].Loss == None
    assert f.Inventory.Segment[0].ReasonForDecommissioning == None
    assert f.Inventory.Segment[0].Reflectance == None
    assert f.Inventory.Segment[0].uid == '10'
    assert f.Inventory.Segment[0].CableType == 'single-fiber-cable'
    assert f.Inventory.Segment[0].CladdedDiameter == None
    assert f.Inventory.Segment[0].Coating == None
    assert f.Inventory.Segment[0].CoreDiameter == None
    assert f.Inventory.Segment[0].CoreType == None
    assert float(f.Inventory.Segment[0].FiberLength.valueOf_) == 25
    assert f.Inventory.Segment[0].FiberLength.uom == 'm'
    assert f.Inventory.Segment[0].Jacket == None
    assert f.Inventory.Segment[0].Mode == None
    assert f.Inventory.Segment[0].OutsideDiameter == None
    assert float(f.Inventory.Segment[0].OverStuffing.valueOf_) == 0
    assert f.Inventory.Segment[0].OverStuffing.uom == 'm'
    assert f.Inventory.Segment[0].Parameter == []
    assert f.Inventory.Segment[0].SpoolLength == None
    assert f.Inventory.Segment[0].SpoolNumberTag == None
    assert f.Inventory.Segment[0].OneWayAttenuation == []
    assert f.Inventory.Segment[0].FiberConveyance == None
    assert f.Inventory.Segment[0].RefractiveIndex == []
    assert f.Inventory.Segment[1].Manufacturer == None
    assert f.Inventory.Segment[1].ManufacturingDate == None
    assert f.Inventory.Segment[1].Name == 'Downhole Fiber'
    assert f.Inventory.Segment[1].SoftwareVersion == None
    assert f.Inventory.Segment[1].SupplierModelNumber == None
    assert f.Inventory.Segment[1].SupplyDate == None
    assert f.Inventory.Segment[1].Type == 'fiber'
    assert f.Inventory.Segment[1].Loss == None
    assert f.Inventory.Segment[1].ReasonForDecommissioning == None
    assert f.Inventory.Segment[1].Reflectance == None
    assert f.Inventory.Segment[1].uid == '30'
    assert f.Inventory.Segment[1].CableType == None
    assert f.Inventory.Segment[1].CladdedDiameter == None
    assert f.Inventory.Segment[1].Coating == None
    assert f.Inventory.Segment[1].CoreDiameter == None
    assert f.Inventory.Segment[1].CoreType == None
    assert float(f.Inventory.Segment[1].FiberLength.valueOf_) == 492.430
    assert f.Inventory.Segment[1].FiberLength.uom == 'm'
    assert f.Inventory.Segment[1].Jacket == None
    assert f.Inventory.Segment[1].Mode == None
    assert f.Inventory.Segment[1].OutsideDiameter == None
    assert float(f.Inventory.Segment[1].OverStuffing.valueOf_) == 9.182
    assert f.Inventory.Segment[1].OverStuffing.uom == 'm'
    assert f.Inventory.Segment[1].Parameter == []
    assert f.Inventory.Segment[1].SpoolLength == None
    assert f.Inventory.Segment[1].SpoolNumberTag == None
    assert f.Inventory.Segment[1].OneWayAttenuation == []
    assert f.Inventory.Segment[1].FiberConveyance.Cable.extensiontype_ == 'prodml:PermanentCable'
    assert f.Inventory.Segment[1].FiberConveyance.Cable.Comment == 'Over stuffing is equally distributed along the installed downhole cable'
    assert f.Inventory.Segment[1].FiberConveyance.Cable.PermanentCableInstallationType == 'clamped to tubular'
    assert f.Inventory.Segment[1].RefractiveIndex == []
    assert f.Inventory.Splice == []
    assert f.Inventory.Terminator.Manufacturer == None
    assert f.Inventory.Terminator.ManufacturingDate == None
    assert f.Inventory.Terminator.Name == 'Main Terminator'
    assert f.Inventory.Terminator.SoftwareVersion == None
    assert f.Inventory.Terminator.SupplierModelNumber == None
    assert f.Inventory.Terminator.SupplyDate == None
    assert f.Inventory.Terminator.Type == None
    assert f.Inventory.Terminator.Loss == None
    assert f.Inventory.Terminator.ReasonForDecommissioning == None
    assert f.Inventory.Terminator.Reflectance == None
    assert f.Inventory.Terminator.uid == '40'
    assert f.Inventory.Terminator.TerminationType == 'termination at cable'
    assert f.Inventory.Turnaround == []
    ## Otdr.
    assert len(f.Otdr) == 1
    assert f.Otdr[0].DataInOTDRFile == 'myOTDR.dat'
    assert f.Otdr[0].Direction == 'forward'
    assert f.Otdr[0].DTimRun == aniso8601.parse_datetime('2005-02-14T09:00:00')
    assert f.Otdr[0].Name == 'Initial OTDR'
    assert float(f.Otdr[0].OpticalPathDistanceEnd.valueOf_) == 517.43
    assert f.Otdr[0].OpticalPathDistanceEnd.uom == 'm'
    assert float(f.Otdr[0].OpticalPathDistanceStart.valueOf_) == 0
    assert f.Otdr[0].OpticalPathDistanceStart.uom == 'm'
    assert f.Otdr[0].OTDRImageFile == None
    assert f.Otdr[0].ReasonForRun == None
    assert f.Otdr[0].uid == 'OTDR1'
    assert float(f.Otdr[0].Wavelength.valueOf_) == 1550
    assert f.Otdr[0].Wavelength.uom == 'nm'
    assert f.Otdr[0].MeasurementContact == None
    assert f.Otdr[0].ExtensionNameValue == []
    assert f.Otdr[0].FiberOTDRInstrumentBox == None

def verify_das_instrument_box(pml):
    """Verify DasInstrumentBox contents."""
    assert pml.das_instrument_box
    b = pml.das_instrument_box # shorthand
    ## Aliases.
    assert len(b.Aliases) == 0
    ## Citation.
    assert b.Citation.Creation == aniso8601.parse_datetime('2015-07-20T01:00:00.000000Z')
    assert b.Citation.Description == None
    assert b.Citation.DescriptiveKeywords == None
    assert b.Citation.Editor == None
    assert b.Citation.Format == 'Vendor:ApplicationName'
    assert b.Citation.LastUpdate == None
    assert b.Citation.Originator == 'Fred Mertz, Field Tech'
    assert b.Citation.Title == 'Instrument Box'
    assert b.Citation.VersionString == None
    ## CustomData.
    assert b.CustomData == None
    ## existenceKind.
    assert b.existenceKind == None
    ## ExtensionNameValue.
    assert len(b.ExtensionNameValue) == 0
    ## objectVersion.
    assert b.objectVersion == None
    ## schemaVersion.
    assert b.schemaVersion == '2.0'
    ## uuid.
    assert b.uuid == 'df9447a1-8c6b-4634-88eb-ab07b41ac876'
    ## FacilityIdentifier.
    assert b.FacilityIdentifier == None
    assert b.FirmwareVersion == 'Firmware version 1'
    assert b.Instrument.Manufacturer == None
    assert b.Instrument.ManufacturingDate == None
    assert b.Instrument.Name == 'Instrument Box'
    assert b.Instrument.SoftwareVersion == None
    assert b.Instrument.SupplierModelNumber == None
    assert b.Instrument.SupplyDate == None
    assert b.Instrument.Type == None
    assert b.Instrument.InstrumentVendor == None
    assert b.InstrumentBoxDescription == None
    assert len(b.Parameter) == 1
    assert b.Parameter[0].description == 'time'
    assert int(b.Parameter[0].index) == 1
    assert b.Parameter[0].name == 'Parameter1'
    assert b.Parameter[0].uom == 's'
    assert b.PatchCord == None
    assert b.SerialNumber == '12645A'

def verify_hdf(pml):
    """Verify hdf files contents."""
    # HDF attributes and Calibration data are verified after opening an EPC file, so we only verify contents here.
    # RawData.
    uuid = pml.das_acquisition.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].EpcExternalPartReference.Uuid
    f = h5py.File(os.path.join(pml.epc_folder, pml.get_hdf_filename(uuid)), 'r')
    data = f[pml.das_acquisition.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].PathInExternalFile]
    assert np.amin(data) == 0.0
    assert np.amax(data) == 1.0
    assert np.mean(data) == pytest.approx(0.11991847)
    # RawDataTime.
    uuid = pml.das_acquisition.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].EpcExternalPartReference.Uuid
    f = h5py.File(os.path.join(pml.epc_folder, pml.get_hdf_filename(uuid)), 'r')
    data = f[pml.das_acquisition.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].PathInExternalFile]
    assert np.amin(data) == 1437351825678000
    assert np.amax(data) == 1437351845658000
    assert np.mean(data) == pytest.approx(1437351825678000)
    # RawDataTriggerTime.
    uuid = pml.das_acquisition.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0].EpcExternalPartReference.Uuid
    f = h5py.File(os.path.join(pml.epc_folder, pml.get_hdf_filename(uuid)), 'r')
    data = f[pml.das_acquisition.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0].PathInExternalFile]
    assert np.mean(data) == 1437351825678000

if __name__ == '__main__':
    pytest.main()
