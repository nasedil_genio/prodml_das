# License notice
#
# Copyright 2017
#
# Energistics
# The following Energistics (c) products were used in the creation of this work:
#
# •             PRODML Data Schema Specifications, Version 1.3.1
# •             PRODML Data Schema Specifications, Version 2.0
#
# All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
# any portion thereof, which remain in the Standards DevKit shall remain with Energistics
# or its suppliers and shall remain subject to the terms of the Product License Agreement
# available at http://www.energistics.org/product-license-agreement.
#
# Apache
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
# except in compliance with the License.
#
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the
# License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the License for the specific language governing permissions and limitations under the
# License.
#
# All rights reserved.
#

import datetime
import os

import h5py
import numpy as np
import pytest

import prodml_das.DasAcquisition as da

from prodml_das.prodml import PMLproxy
from prodml_das import EPC_CT

import test_read_case2

def test_write_testcase2(tmpdir):
    # Make temporary path and write to it.
    filename = str(tmpdir.join('Case2/Case2.epc'))
    # Write out test file which should be the same as test case in `data/Case2/Case2.epc`.
    write_testcase2(filename)
    # Read that test file and verify it.
    test_read_case2.verify_testcase_2(filename)

def write_testcase2(filename):
    """Write testcase with contents same as in `data/Case3/Case3.epc` using prodml library.
    """
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    p = PMLproxy(filename)
    p.das_acquisition = create_das_acquisition()
    p.das_instrument_box = create_das_instrument_box()
    p.fiber_optical_path = create_fiber_optical_path()
    p.save()
    ref_hdf = h5py.File(os.path.join(os.path.dirname(__file__), '../data/Case2/data.h5'), 'r') # reference hdf file
    dst_hdf = h5py.File(p.get_full_hdf_path(p.das_acquisition.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].EpcExternalPartReference.Uuid), 'a')
    pathRawData = p.das_acquisition.Raw[0].RawData.RawDataArray.Values.ExternalFileProxy[0].PathInExternalFile
    pathRawDataTime = p.das_acquisition.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].PathInExternalFile
    pathRawDataTriggerTime = p.das_acquisition.Raw[0].RawDataTriggerTime.TimeArray.Values.ExternalFileProxy[0].PathInExternalFile
    pathFbeData = p.das_acquisition.Processed.Fbe[0].FbeData[0].FbeDataArray.Values.ExternalFileProxy[0].PathInExternalFile
    pathFbeDataTime = p.das_acquisition.Processed.Fbe[0].FbeDataTime.TimeArray.Values.ExternalFileProxy[0].PathInExternalFile
    # Create datasets.
    number_of_loci = p.das_acquisition.NumberOfLoci
    number_of_traces = p.das_acquisition.Raw[0].RawDataTime.TimeArray.Values.ExternalFileProxy[0].Count
    number_of_fbe_timepoints = p.das_acquisition.Processed.Fbe[0].FbeDataTime.TimeArray.Values.ExternalFileProxy[0].Count
    dst_hdf.require_dataset(pathRawData, (number_of_traces, number_of_loci), dtype=np.float32)
    dst_hdf.require_dataset(pathRawDataTime, (number_of_traces, ), dtype=np.int64)
    dst_hdf.require_dataset(pathRawDataTriggerTime, (1, ), dtype=np.int64)
    dst_hdf.require_dataset(pathFbeData, (number_of_fbe_timepoints, number_of_loci), dtype=np.float32)
    dst_hdf.require_dataset(pathFbeDataTime, (number_of_fbe_timepoints, ), dtype=np.int64)
    # Copy data from reference file.
    dst_hdf[pathRawData][:, :] = ref_hdf[pathRawData][:, :]
    dst_hdf[pathRawDataTime][:] = ref_hdf[pathRawDataTime][:]
    dst_hdf[pathRawDataTriggerTime][:] = ref_hdf[pathRawDataTriggerTime][:]
    dst_hdf[pathFbeData][:, :] = ref_hdf[pathFbeData][:, :]
    dst_hdf[pathFbeDataTime][:] = ref_hdf[pathFbeDataTime][:]
    # Close files and update hdf metadata from xml metadata.
    dst_hdf.flush()
    dst_hdf.close()
    ref_hdf.close()
    p.update_hdf_metadata()
    p.save()

def create_das_acquisition():
    das = da.DasAcquisition.factory()
    das.schemaVersion = '2.0'
    das.Aliases = [da.ObjectAlias.factory(authority='abc',
                                          Identifier='My Alias')]
    das.Citation = da.Citation.factory(Title='DAS Acquisition',
                                 Originator='Energistics',
                                 Creation=datetime.datetime.strptime('2015-07-20T01:00:00+0100', '%Y-%m-%dT%H:%M:%S%z'),
                                 Format='Energistics')
    das.CustomData = da.CustomData.factory()
    das.ExtensionNameValue = [da.ExtensionNameValue.factory(Name='customInt',
                                                            Value=da.StringMeasure.factory(valueOf_=2))]
    das.AcquisitionId = 'dc0e381a-094a-4fd2-ab89-dce867e3b99d'
    das.uuid = '9375b1ad-81a6-4126-a556-4e7c6520fc91'
    das.AcquisitionDescription = 'Energistics DAS PRODML Acquisition Sample'
    opticalPath = da.DataObjectReference.factory(Uuid='bfd164f4-f9e3-41c0-a74e-372b69cc2a09', Title='OptPath1', ContentType=EPC_CT.FIBER_OPTICAL_PATH)
    das.OpticalPath = opticalPath
    dasInstrumentBox = da.DataObjectReference.factory(Uuid='df9447a1-8c6b-4634-88eb-ab07b41ac876', Title='Instrument Box', ContentType=EPC_CT.DAS_INSTRUMENT_BOX)
    das.DasInstrumentBox = dasInstrumentBox
    das.FacilityId = ['ABC Facility', 'Well Facility']
    das.VendorCode = da.BusinessAssociate.factory(
        Name="ABCDE",
        Role=[
            da.NameStruct.factory(valueOf_="operator"),
            da.NameStruct.factory(valueOf_="owner"),
        ],
        Alias=[da.NameStruct.factory()],
        Address=da.GeneralAddress.factory(uid='main',
                                  Name='Wyle E Coyote',
                                  Street=['Suite 2100', '515 Congress Avenue'],
                                  City='Austin',
                                  Country='USA',
                                  County='Texas',
                                  PostalCode='78701',
                                  State='Texas',
                                  Province='Texas'),
        PhoneNumber=[da.PhoneNumberStruct.factory(type_='voice', valueOf_='555-555-5555')],
        Email=[da.EmailQualifierStruct.factory()],
        AssociatedWith='',
        Contact=[''],
        PersonName=da.PersonName.factory(
                                 First='Wyle',
                                 Middle='E',
                                 Last='Coyote',
                                 Suffix=['Esq.', 'PhD.'])
    )
    das.PulseRate = da.FrequencyMeasure.factory(uom='Hz',
                                          valueOf_=50.0)
    das.PulseWidth = da.TimeMeasure.factory(uom='ns',
                                      valueOf_=8.0)
    das.PulseWidthUnit = 'ns'
    das.GaugeLength = da.LengthMeasure.factory(uom='m',
                                         valueOf_=40.0)
    das.GaugeLengthUnit = 'm'
    das.SpatialSamplingInterval = da.LengthMeasure.factory(uom='m',
                                                     valueOf_=5.0)
    das.SpatialSamplingIntervalUnit = 'm'
    das.MinimumFrequency = da.FrequencyMeasure.factory(uom='Hz',
                                                 valueOf_=0.5)
    das.MaximumFrequency = da.FrequencyMeasure.factory(uom='Hz',
                                                 valueOf_=25.0)
    das.NumberOfLoci = 5
    das.StartLocusIndex = 0
    das.MeasurementStartTime = '2015-07-20T01:23:45.123456+01:00'
    das.TriggeredMeasurement = False
    epcPartReference1 = da.DataObjectReference.factory(Uuid='672ad2de-f0d7-45a7-9883-53d80b2b02f8', Title='Hdf Proxy', ContentType=EPC_CT.EPC_EXTERNAL_PART_REFERENCE)
    fbe = da.DasFbe.factory(Custom=da.DasCustom.factory(),
                            FbeData=[da.DasFbeData.factory(Dimensions=['time', 'locus'],
                                                           EndFrequency=da.FrequencyMeasure.factory(uom='Hz',
                                                                                                    valueOf_=1.0),
                                                           StartFrequency=da.FrequencyMeasure.factory(uom='Hz',
                                                                                                    valueOf_=0.5),
                                                           FbeDataArray=da.DoubleExternalArray.factory(
                                                               Values=da.ExternalDataset.factory(ExternalFileProxy=[
                                                                   da.DasExternalDatasetPart.factory(Count=500,
                                                                   PathInExternalFile='/Acquisition/Processed/Fbe[0]/FbeData',
                                                                   StartIndex=0,
                                                                   EpcExternalPartReference=epcPartReference1,
                                                                   PartStartTime='2015-07-20T01:23:45.678000+01:00',
                                                                   PartEndTime='2015-07-20T01:23:55.578000+01:00')])))],
                            FbeDataTime=da.DasTimeArray.factory(
                               StartTime='2015-07-20T01:23:45.678000+01:00',
                               EndTime='2015-07-20T01:23:55.578000+01:00',
                               TimeArray=da.IntegerExternalArray.factory(
                                   NullValue=-1,
                                   Values=da.ExternalDataset.factory(
                                       ExternalFileProxy=[
                                           da.DasExternalDatasetPart.factory(
                                               Count=100,
                                               PathInExternalFile='/Acquisition/Processed/Fbe[0]/FbeDataTime',
                                               StartIndex=0,
                                               EpcExternalPartReference=epcPartReference1,
                                               PartStartTime='2015-07-20T01:23:45.678000+01:00',
                                               PartEndTime='2015-07-20T01:23:55.578000+01:00')]))),
                            FbeDataUnit='V',
                            FilterType='BUTTERWORTH',
                            NumberOfLoci=5,
                            OutputDataRate=da.FrequencyMeasure.factory(uom='Hz',
                                                                       valueOf_=10.0),
                            RawReference='0197556b-5642-4e0c-92c5-442281e28277',
                            SpatialSamplingInterval=da.LengthMeasure.factory(uom='m',
                                                                             valueOf_=5.0),
                            SpatialSamplingIntervalUnit='m',
                            StartLocusIndex=10,
                            TransformSize=32,
                            TransformType='FFT',
                            uuid='fd6bbb77-b9bf-45c7-9424-4ef9498693f8',
                            WindowOverlap=16,
                            WindowSize=32)
    das.Processed = da.DasProcessed.factory(Fbe=[fbe], Spectra=[])
    rawCustom = da.DasCustom.factory()
    uuid_raw = '0197556b-5642-4e0c-92c5-442281e28277'
    raw = da.DasRaw.factory(uuid=uuid_raw,
                    RawDataUnit='V',
                    OutputDataRate=da.FrequencyMeasure.factory(uom='Hz',
                                                       valueOf_=50.0),
                    StartLocusIndex=0,
                    NumberOfLoci=5,
                    RawData=da.DasRawData.factory(
                        Dimensions=['time', 'locus'],
                        RawDataArray=da.DoubleExternalArray.factory(
                            Values=da.ExternalDataset.factory(
                                ExternalFileProxy=[
                                    da.DasExternalDatasetPart.factory(Count=5000,
                                                              PathInExternalFile='/Acquisition/Raw[0]/RawData',
                                                              StartIndex=0,
                                                              EpcExternalPartReference=epcPartReference1,
                                                              PartStartTime='2015-07-20T01:23:45.678000+01:00',
                                                              PartEndTime='2015-07-20T01:24:05.658000+01:00')]))),
                    RawDataTime=da.DasTimeArray.factory(
                        StartTime='2015-07-20T01:23:45.678000+01:00',
                        EndTime='2015-07-20T01:24:05.658000+01:00',
                        TimeArray=da.IntegerExternalArray.factory(
                            NullValue=-1,
                            Values=da.ExternalDataset.factory(
                                ExternalFileProxy=[
                                    da.DasExternalDatasetPart.factory(
                                        Count=1000,
                                        PathInExternalFile='/Acquisition/Raw[0]/RawDataTime',
                                        StartIndex=0,
                                        EpcExternalPartReference=epcPartReference1,
                                        PartStartTime='2015-07-20T01:23:45.678000+01:00',
                                        PartEndTime='2015-07-20T01:24:05.658000+01:00')]))),
                    RawDataTriggerTime=da.DasTimeArray.factory(
                        StartTime='2015-07-20T01:23:45.678000+01:00',
                        EndTime='2015-07-20T01:24:05.658000+01:00',
                        TimeArray=da.IntegerExternalArray.factory(
                            NullValue=-1,
                            Values=da.ExternalDataset.factory(ExternalFileProxy=[
                                da.DasExternalDatasetPart.factory(
                                    Count=1,
                                    PathInExternalFile='/Acquisition/Raw[0]/RawDataTriggerTime',
                                    StartIndex=0,
                                    EpcExternalPartReference=epcPartReference1,
                                    PartStartTime='2015-07-20T01:23:45.678000+01:00',
                                    PartEndTime='2015-07-20T01:23:45.678000+01:00'
                                )
                            ]
                            )
                        )
                    ),
                    Custom=rawCustom
    )
    das.Raw = [raw]
    # The rest.
    dasCustom = da.DasCustom.factory()
    dasCustom.original_tagname_ = 'Custom'
    das.Custom = dasCustom
    calibrationDataPoint1 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=4,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=23.500
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=23.500),
        CalibrationType='tap test'
    )
    calibrationDataPoint2 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=101,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=-1.000
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=12.430
        ),
        CalibrationType='last locus to end of fiber'
    )
    calibrationDataPoint3 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=0,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=5.00
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=5.00),
        CalibrationType='locus calibration'
    )
    calibrationDataPoint4 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=1,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=10.00
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=10.00),
        CalibrationType='locus calibration'
    )
    calibrationDataPoint5 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=4,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=25.00
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=25.00),
        CalibrationType='locus calibration'
    )
    calibrationDataPoint6 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=5,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=30.00
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=29.907
        ),
        CalibrationType='locus calibration'
    )
    calibrationDataPoint7 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=6,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=35.00
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=34.814
        ),
        CalibrationType='locus calibration'
    )
    calibrationDataPoint8 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=99,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=500.0),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=491.143
        ),
        CalibrationType='locus calibration'
    )
    calibrationDataPoint9 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=100,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=505.0
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=496.050
        ),
        CalibrationType='locus calibration'
    )
    calibrationDataPoint10 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=5,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=30.0
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=4.907
        ),
        CalibrationType='locus calibration'
    )
    calibrationDataPoint11 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=6,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=35.0
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=9.814),
        CalibrationType='locus calibration'
    )
    calibrationDataPoint12 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=99,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=500.00
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=466.143
        ),
        CalibrationType='locus calibration'
    )
    calibrationDataPoint13 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=100,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=505.00
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=471.05
        ),
        CalibrationType='locus calibration'
    )
    calibrationDataPoint14 =  da.DasCalibrationPoint.factory(
        CalibrationLocusIndex=100,
        CalibrationOpticalPathDistance=da.LengthMeasure.factory(
            uom='m',
            valueOf_=12.43
        ),
        CalibrationFacilityLength=da.LengthMeasure.factory(
            uom='m',
            valueOf_=-1.0
        ),
        CalibrationType='last locus to end of fiber'
    )
    das.Calibration = [
            da.DasCalibration.factory(
                NumberOfCalibrationPoints=9,
                FacilityName='Facility name',
                FacilityKind='generic',
                CalibrationDataPoints=[
                    calibrationDataPoint1,
                    calibrationDataPoint2,
                    calibrationDataPoint3,
                    calibrationDataPoint4,
                    calibrationDataPoint5,
                    calibrationDataPoint6,
                    calibrationDataPoint7,
                    calibrationDataPoint8,
                    calibrationDataPoint9
                ]
            ),
            da.DasCalibration.factory(
                NumberOfCalibrationPoints=5,
                FacilityName='Facility name',
                FacilityKind='generic',
                CalibrationDataPoints=[
                    calibrationDataPoint10,
                    calibrationDataPoint11,
                    calibrationDataPoint12,
                    calibrationDataPoint13,
                    calibrationDataPoint14
                ]
            )
        ]
    return das

def create_das_instrument_box():
    dib = da.DasInstrumentBox.factory(
        SerialNumber='12645A',
        Parameter=[
            da.IndexedObject.factory(
                index=1,
                name='Parameter1',
                uom='s',
                description='time'
            )
        ],
        Instrument=da.Instrument.factory(Name='Instrument Box'),
        FirmwareVersion='Firmware version 1'
    )
    dib.Citation = da.Citation.factory(
            Title='Instrument Box',
            Originator='Fred Mertz, Field Tech',
            Creation=datetime.datetime.strptime('2015-07-20T01:00:00.000000+0000', '%Y-%m-%dT%H:%M:%S.%f%z'),
            Format='Vendor:ApplicationName')
    dib.schemaVersion = '2.0'
    dib.uuid = 'df9447a1-8c6b-4634-88eb-ab07b41ac876'
    return dib

def create_fiber_optical_path():
    fop = da.FiberOpticalPath.factory(
        Inventory=da.FiberOpticalPathInventory.factory(
            Connection=[
                da.FiberConnection.factory(
                    uid='20',
                    Name='Surface Connector',
                    Type='connector',
                    ConnectorType='dry mate'
                )
            ],
            Segment=[
                da.FiberOpticalPathSegment.factory(
                    uid='10',
                    Name='Surface Fiber',
                    Type='fiber',
                    FiberLength=da.LengthMeasure.factory(
                        uom='m',
                        valueOf_=25
                    ),
                    OverStuffing=da.LengthMeasure.factory(
                        uom='m',
                        valueOf_=0
                    ),
                    CableType='single-fiber-cable'
                ),
                da.FiberOpticalPathSegment.factory(
                    uid='30',
                    Name='Downhole Fiber',
                    Type='fiber',
                    FiberLength=da.LengthMeasure.factory(
                        uom='m',
                        valueOf_=492.430
                    ),
                    OverStuffing=da.LengthMeasure.factory(
                        uom='m',
                        valueOf_=9.182
                    ),
                    FiberConveyance=da.FiberConveyance.factory(
                        Cable=da.PermanentCable.factory(
                            PermanentCableInstallationType='clamped to tubular',
                            Comment='Over stuffing is equally distributed along the installed downhole cable'
                        )
                    )
                )
            ],
            Terminator=da.FiberTerminator.factory(
                uid='40',
                Name='Main Terminator',
                TerminationType='termination at cable'
            )
        ),
        OpticalPathNetwork=[
            da.FiberOpticalPathNetwork.factory(
                uid='OPN1',
                ContextFacility=[
                    da.FacilityIdentifierStruct.factory(
                        valueOf_='text'
                    )
                ],
                Network=[
                    da.ProductFlowNetwork.factory(
                        uid='N1',
                        Name='Current Setup Well-01',
                        Unit=[
                            da.ProductFlowUnit.factory(
                                uid='OPNU10',
                                Facility=da.FacilityIdentifierStruct.factory(
                                    uidRef=10,
                                    valueOf_='Surface Fiber Segment'
                                ),
                                Port=[
                                    da.ProductFlowPort.factory(
                                        uid='1',
                                        Direction='inlet',
                                        Name='Connection to LightBox',
                                        ConnectedNode=[
                                            da.ConnectedNode.factory(
                                                uid='CN',
                                                Node='Instrument Box'
                                            )
                                        ]
                                    ),
                                    da.ProductFlowPort.factory(
                                        uid='2',
                                        Direction='outlet',
                                        Name='Connection to Surface Connector',
                                        ConnectedNode=[
                                            da.ConnectedNode.factory(
                                                uid='CSC1',
                                                Node='Surface Connector 1'
                                            )
                                        ]
                                    )
                                ]
                            ),
                            da.ProductFlowUnit.factory(
                                uid='OPNU20',
                                Facility=da.FacilityIdentifierStruct.factory(
                                    uidRef=20,
                                    valueOf_='Surface Connector'
                                ),
                                Port=[
                                    da.ProductFlowPort.factory(
                                        uid='3',
                                        Direction='inlet',
                                        Name='Connection from Surface Connector to Surface Fiber',
                                        ConnectedNode=[
                                            da.ConnectedNode.factory(
                                                uid='CSC1',
                                                Node='Surface Connector 1'
                                            )
                                        ]
                                    ),
                                    da.ProductFlowPort.factory(
                                        uid='4',
                                        Direction='outlet',
                                        Name='Connection from Surface Connector to Downhole Fiber',
                                        ConnectedNode=[
                                            da.ConnectedNode.factory(
                                                uid='CDF1',
                                                Node='Surface Connector 2'
                                            )
                                        ]
                                    )
                                ]
                            ),
                            da.ProductFlowUnit.factory(
                                uid='OPNU30',
                                Facility=da.FacilityIdentifierStruct.factory(
                                    uidRef=30,
                                    valueOf_='Downhole Fiber'
                                ),
                                Port=[
                                    da.ProductFlowPort.factory(
                                        uid='5',
                                        Direction='inlet',
                                        Name='Connection from Surface Connector to Downhole Fiber',
                                        ConnectedNode=[
                                            da.ConnectedNode.factory(
                                                uid='CDF1',
                                                Node='Surface Connector 2'
                                            )
                                        ]
                                    ),
                                    da.ProductFlowPort.factory(
                                        uid='6',
                                        Direction='outlet',
                                        Name='Connection from Downhole Fiber to Terminator',
                                        ConnectedNode=[
                                            da.ConnectedNode.factory(
                                                uid='CT1',
                                                Node='Downhole Connection 1'
                                            )
                                        ]
                                    )
                                ]
                            ),
                            da.ProductFlowUnit.factory(
                                uid='OPNU40',
                                Facility=da.FacilityIdentifierStruct.factory(
                                    valueOf_='Terminator'
                                ),
                                Port=[
                                    da.ProductFlowPort.factory(
                                        uid='7',
                                        Direction='inlet',
                                        Name='Connection from Downhole Fiber to Terminator',
                                        ConnectedNode=[da.ConnectedNode.factory(
                                            uid='CT1',
                                            Node='Downhole Connection 1'
                                        )
                                        ]
                                    )
                                ]
                            )
                        ]
                    )
                ]
            )
        ],
        FacilityMapping=[
            da.FiberFacilityMapping.factory(
                uid='FM1',
                TimeStart=datetime.datetime.strptime(
                    '2005-07-20T01:00:00', '%Y-%m-%dT%H:%M:%S'),
                FiberFacilityMappingPart=[
                    da.FiberFacilityMappingPart.factory(
                        uid='FMP1',
                        OpticalPathDistanceStart=da.LengthMeasure.factory(
                            uom='m',
                            valueOf_=0.0
                        ),
                        OpticalPathDistanceEnd=da.LengthMeasure.factory(
                            uom='m',
                            valueOf_=25.0
                        ),
                        FacilityLengthStart=da.LengthMeasure.factory(
                            uom='m',
                            valueOf_=0.0
                        ),
                        FacilityLengthEnd=da.LengthMeasure.factory(
                            uom='m',
                            valueOf_=25.0
                        ),
                        Comment="No 'timeEnd' specified since this mapping is still valid. No overstuffing in surface cable",
                        FiberFacility=da.FiberFacilityGeneric.factory(
                            FacilityName='Surface Cable 1',
                            FacilityKind='Surface Cable'
                        )
                    ),
                    da.FiberFacilityMappingPart.factory(
                        uid='FMP2',
                        OpticalPathDistanceStart=da.LengthMeasure.factory(
                            uom='m',
                            valueOf_=25.0
                        ),
                        OpticalPathDistanceEnd=da.LengthMeasure.factory(
                            uom='m',
                            valueOf_=517.43
                        ),
                        FacilityLengthStart=da.LengthMeasure.factory(
                            uom='m',
                            valueOf_=0.0
                        ),
                        FacilityLengthEnd=da.LengthMeasure.factory(
                            uom='m',
                            valueOf_=483.25
                        ),
                        Comment="No 'timeEnd' specified since this mapping is still valid. The over stuffing of 9.182 m equally distributed along the installed downhole cable",
                        FiberFacility=da.FiberFacilityWell.factory(
                            Name='Well-01 Fiber',
                            WellDatum='kelly bushing',
                            WellboreReference=da.DataObjectReference.factory(
                                ContentType='application/x-witsml+xml;version=2.0;type=wellbore',
                                Title='Main wellbore of well Well-01',
                                Uuid='4ab0fae6-dc0e-405a-b812-203a154c1243'
                            )
                        )
                    )
                ]
            )
        ],
        Defect=[
            da.FiberPathDefect.factory(
                defectID='OPTDEFECT1',
                OpticalPathDistanceStart=da.LengthMeasure.factory(
                    uom='ft',
                    valueOf_=352.00
                ),
                DefectType='other',
                TimeStart=datetime.datetime.strptime(
                    '2005-07-20T01:00:00',
                    '%Y-%m-%dT%H:%M:%S'
                ),
                Comment='Consistent signal spike detected at this location'
            )
        ],
        Otdr=[
            da.FiberOTDR.factory(
                uid='OTDR1',
                Name='Initial OTDR',
                DTimRun=datetime.datetime.strptime('2005-02-14T09:00:00', '%Y-%m-%dT%H:%M:%S'),
                DataInOTDRFile='myOTDR.dat',
                OpticalPathDistanceStart=da.LengthMeasure.factory(
                    uom='m',
                    valueOf_=0.0
                ),
                OpticalPathDistanceEnd=da.LengthMeasure.factory(
                    uom='m',
                    valueOf_=517.43
                ),
                Direction='forward',
                Wavelength=da.LengthMeasure.factory(
                    uom='nm',
                    valueOf_=1550
                )
            )
        ],
        InstallingVendor=da.BusinessAssociate.factory(
            Name='Vendor Name',
            Contact=['Mr A Vendor']
        ),
        FacilityIdentifier=da.FacilityIdentifier.factory(
            uid='',
            Name=da.NameStruct(
                valueOf_='Well-01'
            )
        )
    )

    fop.Citation = da.Citation.factory(
            Title='OptPath1',
            Originator='Source',
            Creation=datetime.datetime.strptime('2005-07-20T01:00:00', '%Y-%m-%dT%H:%M:%S'),
            Format='1',
            Description='FiberOpticalPath DAS worked example'
        )
    fop.schemaVersion = '2.0'
    fop.uuid = 'bfd164f4-f9e3-41c0-a74e-372b69cc2a09'
    return fop


if __name__ == '__main__':
    pytest.main()
