# License notice
#
# Copyright 2017
#
# Energistics
# The following Energistics (c) products were used in the creation of this work:
#
# •             PRODML Data Schema Specifications, Version 1.3.1
# •             PRODML Data Schema Specifications, Version 2.0
#
# All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
# any portion thereof, which remain in the Standards DevKit shall remain with Energistics
# or its suppliers and shall remain subject to the terms of the Product License Agreement
# available at http://www.energistics.org/product-license-agreement.
#
# Apache
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
# except in compliance with the License.
#
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the
# License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the License for the specific language governing permissions and limitations under the
# License.
#
# All rights reserved.
#

import datetime
import os

import h5py
import numpy as np
import pytest

import prodml_das.DasAcquisition as da

from prodml_das.prodml import PMLproxy
from prodml_das import EPC_CT

def test_minimal(tmpdir):
    """Make an EPC file with minimal set of parameters (all which are required).
    """
    filename = str(tmpdir.join('minimal/minimal.epc'))
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    p = PMLproxy(filename)
    p.das_acquisition = create_das_acquisition()
    p.das_instrument_box = create_das_instrument_box()
    p.fiber_optical_path = create_fiber_optical_path()
    p.save()

def create_das_acquisition():
    das = da.DasAcquisition.factory()
    das.AcquisitionId='dc0e381a-094a-4fd2-ab89-dce867e3b99a'
    das.Citation = da.Citation.factory(
        Title='DAS Acquisition',
        Originator='Energistics',
        Creation=datetime.datetime.strptime('2015-07-20T01:00:00+0100', '%Y-%m-%dT%H:%M:%S%z'),
        Format='Energistics'
    )
    das.DasInstrumentBox=da.DataObjectReference.factory(Uuid='dc0e381a-094a-4fd2-ab89-dce867e3b99d', Title='DasInstrumentBox', ContentType=EPC_CT.DAS_INSTRUMENT_BOX)
    das.FacilityId = ['Well Facility']
    das.GaugeLength = da.LengthMeasure.factory(uom='m',
                                               valueOf_=40.0)
    das.MaximumFrequency = da.FrequencyMeasure.factory(uom='Hz',
                                                       valueOf_=25.0)
    das.MeasurementStartTime = '2015-07-20T01:23:45.123456+01:00'
    das.MinimumFrequency = da.FrequencyMeasure.factory(uom='Hz',
                                                       valueOf_=0.5)
    das.NumberOfLoci = 5
    das.OpticalPath=da.DataObjectReference.factory(Uuid='dc0e381a-094a-4fd2-ab89-dce867e3b99f', Title='FiberOpticalPath', ContentType=EPC_CT.FIBER_OPTICAL_PATH)
    das.PulseRate = da.FrequencyMeasure.factory(uom='Hz',
                                                valueOf_=50.0)
    das.PulseWidth = da.TimeMeasure.factory(uom='ns',
                                            valueOf_=8.0)
    das.schemaVersion = '2.0'
    das.SpatialSamplingInterval = da.LengthMeasure.factory(uom='m',
                                                           valueOf_=5.0)
    das.StartLocusIndex = 0
    das.TriggeredMeasurement = False
    das.uuid = 'dc0e381a-094a-4fd2-ab89-dce867e3b98a'
    das.VendorCode = da.BusinessAssociate.factory(
        Name="ABCDE"
    )
    return das

def create_das_instrument_box():
    dib = da.DasInstrumentBox.factory()
    dib.Citation = da.Citation.factory(
        Title='Instrument Box',
        Originator='Energistics',
        Creation=datetime.datetime.strptime('2015-07-20T01:00:00.000000+0000', '%Y-%m-%dT%H:%M:%S.%f%z'),
        Format='Energistics')
    dib.FirmwareVersion = 'Firmware version 1'
    dib.Instrument = da.Instrument.factory(Name='Instrument Box')
    dib.schemaVersion = '2.0'
    dib.uuid = 'dc0e381a-094a-4fd2-ab89-dce867e3b99d'
    return dib

def create_fiber_optical_path():
    fop = da.FiberOpticalPath.factory()
    fop.Citation = da.Citation.factory(
        Title='FiberOpticalPath',
        Originator='Energistics',
        Creation=datetime.datetime.strptime('2005-07-20T01:00:00', '%Y-%m-%dT%H:%M:%S'),
        Format='Energistics'
    )
    fop.Inventory = da.FiberOpticalPathInventory.factory(
        Segment=[
            da.FiberOpticalPathSegment.factory(
                uid='10',
                Name='Surface Fiber',
                Type='fiber',
                FiberLength=da.LengthMeasure.factory(uom='m', valueOf_=25)
            )
        ],
        Terminator=da.FiberTerminator.factory(
            uid='40',
            Name='Main Terminator',
            TerminationType='termination at cable'
        )
    )
    fop.schemaVersion = '2.0'
    fop.uuid = 'dc0e381a-094a-4fd2-ab89-dce867e3b99f'
    return fop

if __name__ == '__main__':
    pytest.main()
