# License notice
#
# Copyright 2017
#
# Energistics
# The following Energistics (c) products were used in the creation of this work:
#
# •             PRODML Data Schema Specifications, Version 1.3.1
# •             PRODML Data Schema Specifications, Version 2.0
#
# All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
# any portion thereof, which remain in the Standards DevKit shall remain with Energistics
# or its suppliers and shall remain subject to the terms of the Product License Agreement
# available at http://www.energistics.org/product-license-agreement.
#
# Apache
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
# except in compliance with the License.
#
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the
# License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the License for the specific language governing permissions and limitations under the
# License.
#
# All rights reserved.
#

import os

class EPC_CT:
    """EPC content types.
    """
    DAS_ACQUISITION = 'application/x-prodml+xml;version=2.0;type=DasAcquisition'
    DAS_INSTRUMENT_BOX = 'application/x-prodml+xml;version=2.0;type=DasInstrumentBox'
    EPC_EXTERNAL_PART_REFERENCE = 'application/x-eml+xml;version=2.1;type=EpcExternalPartReference'
    FIBER_OPTICAL_PATH = 'application/x-prodml+xml;version=2.0;type=FiberOpticalPath'


class EPC_RT:
    """EPC relationship types.
    """
    DESTINATION_OBJECT = 'http://schemas.energistics.org/package/2012/relationships/destinationObject'
    SOURCE_OBJECT = 'http://schemas.energistics.org/package/2012/relationships/sourceObject'
    ML_TO_EXTERNAL_PART_PROXY = 'http://schemas.energistics.org/package/2012/relationships/mlToExternalPartProxy'
    EXTERNAL_PART_PROXY_TO_ML = 'http://schemas.energistics.org/package/2012/relationships/externalPartProxyToMl'
    EXTERNAL_RESOURCE = 'http://schemas.energistics.org/package/2012/relationships/externalResource'
    DESTINATION_MEDIA = 'http://schemas.energistics.org/package/2012/relationships/destinationMedia'
    SOURCE_MEDIA = 'http://schemas.energistics.org/package/2012/relationships/sourceMedia'
    CHUNKED_PART = 'http://schemas.energistics.org/package/2012/relationships/chunkedPart'

class PRODML_SCHEMA:
    """ProdML XML schema."""
    # os.path.dirname() is used twice to avoid using '..' in path to xsd, which lead to error when reading schema by lxml.
    DAS_ACQUISITION = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'energistics', 'prodml', 'v2.0', 'xsd_schemas', 'DasAcquisition.xsd')
    FIBER_OPTICAL_PATH = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'energistics', 'prodml', 'v2.0', 'xsd_schemas', 'FiberOpticalPath.xsd')
    EPC_EXTERNAL_PART_REFERENCE = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'energistics', 'common', 'v2.1', 'xsd_schemas', 'EmlAllObjects.xsd')
