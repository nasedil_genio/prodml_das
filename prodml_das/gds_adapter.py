# License notice
#
# Copyright 2017
#
# Energistics
# The following Energistics (c) products were used in the creation of this work:
#
# •             PRODML Data Schema Specifications, Version 1.3.1
# •             PRODML Data Schema Specifications, Version 2.0
#
# All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
# any portion thereof, which remain in the Standards DevKit shall remain with Energistics
# or its suppliers and shall remain subject to the terms of the Product License Agreement
# available at http://www.energistics.org/product-license-agreement.
#
# Apache
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
# except in compliance with the License.
#
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the
# License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the License for the specific language governing permissions and limitations under the
# License.
#
# All rights reserved.
#

import sys
from optparse import OptionParser
from xml.dom import minidom

import prodml_das.DasAcquisition as da

### Utilites to find root tag.

def get_root_tag(node):
    tag = da.Tag_pattern_.match(node.tag).groups()[-1]
    rootClass = None
    if hasattr(da, tag):
        rootClass = getattr(da, tag)
    return tag, rootClass

def parse(inFileName, silence=False):
    parser = None
    doc = da.parsexml_(inFileName, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    doc = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='xmlns:prodml="http://www.energistics.org/energyml/data/prodmlv2"',
            pretty_print=True)
    return rootObj

### Utility mixins

### EmlMixin class

class EmlMixin(object):
    """Changes xml namespace to 'eml:'.
    """
    def export(self, *args, **kwargs):
        if 'prodml:' in args:
            args = list(args)
            args[args.index('prodml:')] = 'eml:'
        elif not 'eml:' in args:
            kwargs['namespace_'] = 'eml:'
        super().export(*args, **kwargs)

    def exportChildren(self, *args, **kwargs):
        if 'prodml:' in args:
            args = list(args)
            args[args.index('prodml:')] = 'eml:'
        elif not 'eml:' in args:
            kwargs['namespace_'] = 'eml:'
        super().exportChildren(*args, **kwargs)

###

### EmlMixinChildren class

class EmlMixinChildren(object):
    """Changes xml namespace to 'eml:'.
    """
    def exportChildren(self, *args, **kwargs):
        if 'prodml:' in args:
            args = list(args)
            args[args.index('prodml:')] = 'eml:'
        elif not 'eml:' in args:
            kwargs['namespace_'] = 'eml:'
        super().exportChildren(*args, **kwargs)

###

### XsiType class

def XsiType(typestring):
    class XsiTypeParametrized(object):
        """Outputs type attribute.
        """
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.extensiontype_ = typestring
    return XsiTypeParametrized

###

### Redefinitions to deal with namespace prefix and xsi:type issues

def DasExternalDatasetPart_exportChildren(self, outfile, level, namespace_='prodml:', name_='DasExternalDatasetPart', fromsubclass_=False, pretty_print=True):
    super(da.DasExternalDatasetPart, self).exportChildren(outfile, level, namespace_, name_, True, pretty_print=pretty_print)
    namespace_ = 'prodml:'
    if pretty_print:
        eol_ = '\n'
    else:
        eol_ = ''
    if self.PartStartTime is not None:
        da.showIndent(outfile, level, pretty_print)
        outfile.write('<%sPartStartTime>%s</%sPartStartTime>%s' % (namespace_, self.gds_encode(self.gds_format_string(da.quote_xml(self.PartStartTime), input_name='PartStartTime')), namespace_, eol_))
    if self.PartEndTime is not None:
        da.showIndent(outfile, level, pretty_print)
        outfile.write('<%sPartEndTime>%s</%sPartEndTime>%s' % (namespace_, self.gds_encode(self.gds_format_string(da.quote_xml(self.PartEndTime), input_name='PartEndTime')), namespace_, eol_))
da.DasExternalDatasetPart.exportChildren = DasExternalDatasetPart_exportChildren

class CitationSub(EmlMixin, da.Citation):
    def __init__(self, Title=None, Originator=None, Creation=None, Format=None, Editor=None, LastUpdate=None, VersionString=None, Description=None, DescriptiveKeywords=None):
        super(CitationSub, self).__init__(Title, Originator, Creation, Format, Editor, LastUpdate, VersionString, Description, DescriptiveKeywords, )
da.Citation.subclass = CitationSub
# end class CitationSub


class CustomDataSub(EmlMixin, da.CustomData):
    def __init__(self, anytypeobjs_=None):
        super(CustomDataSub, self).__init__(anytypeobjs_, )
da.CustomData.subclass = CustomDataSub
# end class CustomDataSub


class ExtensionNameValueSub(EmlMixin, da.ExtensionNameValue):
    def __init__(self, Name=None, Value=None, MeasureClass=None, DTim=None, Index=None, Description=None):
        super(ExtensionNameValueSub, self).__init__(Name, Value, MeasureClass, DTim, Index, Description, )
da.ExtensionNameValue.subclass = ExtensionNameValueSub
# end class ExtensionNameValueSub


class ObjectAliasSub(EmlMixin, da.ObjectAlias):
    def __init__(self, authority=None, Identifier=None, Description=None):
        super(ObjectAliasSub, self).__init__(authority, Identifier, Description, )
da.ObjectAlias.subclass = ObjectAliasSub
# end class ObjectAliasSub

class FiberFacilityGenericSub(XsiType('prodml:FiberFacilityGeneric'), da.FiberFacilityGeneric):
    def __init__(self, FacilityName=None, FacilityKind=None):
        super(FiberFacilityGenericSub, self).__init__(FacilityName, FacilityKind, )
da.FiberFacilityGeneric.subclass = FiberFacilityGenericSub
# end class FiberFacilityGenericSub

class FiberFacilityWellSub(XsiType('prodml:FiberFacilityWell'), da.FiberFacilityWell):
    def __init__(self, Name=None, WellDatum=None, WellboreReference=None):
        super(FiberFacilityWellSub, self).__init__(Name, WellDatum, WellboreReference, )
da.FiberFacilityWell.subclass = FiberFacilityWellSub
# end class FiberFacilityWellSub

class PermanentCableSub(XsiType('prodml:PermanentCable'), da.PermanentCable):
    def __init__(self, PermanentCableInstallationType=None, Comment=None):
        super(PermanentCableSub, self).__init__(PermanentCableInstallationType, Comment, )
da.PermanentCable.subclass = PermanentCableSub
# end class PermanentCableSub

class FacilityIdentifierSub(XsiType('prodml:FacilityIdentifier'), da.FacilityIdentifier):
    def __init__(self, uid=None, Name=None, Installation=None, Kind=None, ContextFacility=None, BusinessUnit=None, Operator=None, GeographicContext=None, valueOf_=None, mixedclass_=None, content_=None):
        super(FacilityIdentifierSub, self).__init__(uid, Name, Installation, Kind, ContextFacility, BusinessUnit, Operator, GeographicContext, valueOf_, mixedclass_, content_, )
da.FacilityIdentifier.subclass = FacilityIdentifierSub
# end class FacilityIdentifierSub

class DataObjectReferenceSub(EmlMixinChildren, da.DataObjectReference):
    def __init__(self, ContentType=None, Title=None, Uuid=None, UuidAuthority=None, Uri=None, VersionString=None):
        super(DataObjectReferenceSub, self).__init__(ContentType, Title, Uuid, UuidAuthority, Uri, VersionString, )
da.DataObjectReference.subclass = DataObjectReferenceSub
# end class DataObjectReferenceSub


class EpcExternalPartReferenceSub(EmlMixin, da.EpcExternalPartReference):
    def __init__(self, Filename=None, MimeType=None):
        super(EpcExternalPartReferenceSub, self).__init__(Filename, MimeType, )
da.EpcExternalPartReference.subclass = EpcExternalPartReferenceSub
# end class EpcExternalPartReferenceSub


class ExternalDatasetSub(EmlMixinChildren, da.ExternalDataset):
    def __init__(self, ExternalFileProxy=None):
        super(ExternalDatasetSub, self).__init__(ExternalFileProxy, )
da.ExternalDataset.subclass = ExternalDatasetSub
# end class ExternalDatasetSub


class ExternalDatasetPartSub(EmlMixin, da.ExternalDatasetPart):
    def __init__(self, Count=None, PathInExternalFile=None, StartIndex=None, EpcExternalPartReference=None, extensiontype_=None):
        super(ExternalDatasetPartSub, self).__init__(Count, PathInExternalFile, StartIndex, EpcExternalPartReference, extensiontype_, )
da.ExternalDatasetPart.subclass = ExternalDatasetPartSub
# end class ExternalDatasetPartSub

class DasExternalDatasetPartSub(XsiType('prodml:DasExternalDatasetPart'), EmlMixin, da.DasExternalDatasetPart):
    def __init__(self, Count=None, PathInExternalFile=None, StartIndex=None, EpcExternalPartReference=None, PartStartTime=None, PartEndTime=None):
        super(DasExternalDatasetPartSub, self).__init__(Count, PathInExternalFile, StartIndex, EpcExternalPartReference, PartStartTime, PartEndTime, )
da.DasExternalDatasetPart.subclass = DasExternalDatasetPartSub
# end class DasExternalDatasetPartSub

class IntegerExternalArraySub(XsiType('eml:IntegerExternalArray'), EmlMixinChildren, da.IntegerExternalArray):
    def __init__(self, NullValue=None, Values=None):
        super(IntegerExternalArraySub, self).__init__(NullValue, Values, )
da.IntegerExternalArray.subclass = IntegerExternalArraySub
# end class IntegerExternalArraySub

class DoubleExternalArraySub(XsiType('eml:DoubleExternalArray'), EmlMixinChildren, da.DoubleExternalArray):
    def __init__(self, Values=None):
        super(DoubleExternalArraySub, self).__init__(Values, )
da.DoubleExternalArray.subclass = DoubleExternalArraySub
# end class DoubleExternalArraySub

###

#fpsub.CitationSub = type(fpsub.CitationSub.__name__, tuple([EmlMixin]+list(fpsub.CitationSub.__bases__)), dict(fpsub.CitationSub.__dict__))
