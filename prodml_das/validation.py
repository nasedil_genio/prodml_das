# License notice
#
# Copyright 2017
#
# Energistics
# The following Energistics (c) products were used in the creation of this work:
#
# •             PRODML Data Schema Specifications, Version 1.3.1
# •             PRODML Data Schema Specifications, Version 2.0
#
# All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
# any portion thereof, which remain in the Standards DevKit shall remain with Energistics
# or its suppliers and shall remain subject to the terms of the Product License Agreement
# available at http://www.energistics.org/product-license-agreement.
#
# Apache
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
# except in compliance with the License.
#
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the
# License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the License for the specific language governing permissions and limitations under the
# License.
#
# All rights reserved.
#

import copy
import datetime as dt
from functools import reduce
import logging
import math
import numbers
import operator
import os
import re
import uuid

import h5py
from lxml import etree
from lxml import objectify
import numpy as np

from prodml_das.constants import EPC_CT, EPC_RT, PRODML_SCHEMA
from prodml_das.utils import parent_hdf_path

### Real values tolerances

# for difference between XML and HDF values.
TOLERANCE_FLOAT_REL = 0.001
TOLERANCE_FLOAT_ABS = 0.001

# for difference between time value steps.
TOLERANCE_TIME_ABS = 1.0

###

def make_attrsRaw():
    attrsRaw = {
        'NumberOfLoci': [True, 'int'],
        'RawDataUnit': [True, 'str'],
        'StartLocusIndex': [True, 'int'],
        'uuid': [True, 'str'],
        'OutputDataRate': [False, 'float'],
        'RawDescription': [False, 'str'],
        'RawIndex': [False, 'int'],
    }
    return attrsRaw

def make_attrsData():
    attrsData = {
        'Count': [True, 'int'],
        'Dimensions': [True, 'strarray'],
        'PartStartTime': [True, 'str'],
        'PartEndTime': [True, 'str'],
        'StartIndex': [True, 'int']
    }
    return attrsData

def make_attrsDataProcessed():
    attrsProcessedData = make_attrsData()
    attrsProcessedData.update({
        'EndFrequency': [True, 'float'],
        'StartFrequency': [True, 'float']
    })
    return attrsProcessedData

def make_attrsDataTime():
    attrsDataTime = {
        'Count': [True, 'int'],
        'PartStartTime': [True, 'str'],
        'PartEndTime': [True, 'str'],
        'StartTime': [True, 'timestamp'],
        'EndTime': [True, 'timestamp'],
        'StartIndex': [True, 'int']
    }
    return attrsDataTime

def make_attrsProcessed():
    attrsProcessed = {
        'OutputDataRate': [True, 'float'],
        'StartLocusIndex': [True, 'int'],
        'NumberOfLoci': [True, 'int'],
        'uuid': [True, 'str'],
        'FilterType': [False, 'str'],
        'SpatialSamplingInterval': [False, 'float'],
        'SpatialSamplingIntervalUnit': [False, 'str'],
        'TransformType': [False, 'str'],
        'TransformSize': [False, 'int'],
        'RawReference': [False, 'str'],
        'WindowFunction': [False, 'str'],
        'WindowSize': [False, 'int'],
        'WindowOverlap': [False, 'int']
    }
    return attrsProcessed

def make_attrsFbe():
    attrsFbe = make_attrsProcessed()
    attrsFbe.update({
        'FbeDataUnit': [True, 'str'],
        'FbeIndex': [False, 'int'],
        'FbeDescription': [False, 'str'],
        'SpectraReference': [False, 'str']
    })
    return attrsFbe

def make_attrsSpectra():
    attrsSpectra = make_attrsProcessed()
    attrsSpectra.update({
        'SpectraDataUnit': [True, 'str'],
        'SpectraIndex': [False, 'int'],
        'SpectraDescription': [False, 'str'],
        'FbeReference': [False, 'str']
    })
    return attrsSpectra

def make_attrsCalibration():
    attrsCalibration = {
        'CalibrationDatum': [False, 'str'],
        'CalibrationDescription': [False, 'str'],
        'CalibrationFacilityLengthUnit': [False, 'str'],
        'CalibrationIndex': [False, 'int'],
        'CalibrationOpticalPathDistanceUnit': [False, 'str'],
        'FacilityKind': [True, 'str'],
        'FacilityName': [True, 'str'],
        'NumberOfCalibrationPoints': [True, 'int']
    }
    return attrsCalibration

def is_valid_uuid(s):
    """Return True if `s` is a valid energistics uuid."""
    if isinstance(s, bytes):
        s = s.decode()
    return re.match("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$", s) is not None

def parse_timestamp(s):
    """Parse timestamp string and return datetime value.
    """
    if s[-3:-2] == ":":
        s = s[:-3] + s[-2:]
    return dt.datetime.strptime(s, '%Y-%m-%dT%H:%M:%S.%f%z')

def validate_xml(xml_content, xsd_file):
    """Return true if an XML file is valid against an XSD schema.
    Raise etree.XMLSyntaxError if there are missing elements.

    Parameters
    ----------
    xml_content : bytes
        XML data to validate
    xsd_file : str
        Path to the XSD schema file
    """
    schema = etree.XMLSchema(file=xsd_file)
    parser = etree.XMLParser(schema=schema)
    if type(xml_content) == str:
        xml_content = xml_content.encode()
    try:
        root = etree.fromstring(xml_content, parser=parser)
    except etree.XMLSyntaxError as e:
        raise e
    if not root.getroottree().docinfo.standalone:
        logging.warning('The document is not standalone.')
    return True

def validate_das_acquisition(das_acquisition):
    """Raise an exception if das_acquisition has errors."""
    if das_acquisition.OpticalPath.ContentType != EPC_CT.FIBER_OPTICAL_PATH:
        raise ValueError('Content Type error: {} is not equal to {}'.format(das_acquisition.OpticalPath.ContentType, EPC_CT.FIBER_OPTICAL_PATH))
    if das_acquisition.DasInstrumentBox.ContentType != EPC_CT.DAS_INSTRUMENT_BOX:
        raise ValueError('Content Type error: {} is not equal to {}'.format(das_acquisition.DasInstrumentBox.ContentType, EPC_CT.DAS_INSTRUMENT_BOX))
    for Raw in das_acquisition.Raw:
        trace_count_rawdata = 0
        for DasExternalDatasetPart in Raw.RawData.RawDataArray.Values.ExternalFileProxy:
            eepr = DasExternalDatasetPart.EpcExternalPartReference
            if eepr.ContentType != EPC_CT.EPC_EXTERNAL_PART_REFERENCE:
                raise ValueError('Content Type error: {} is not equal to {}'.format(eepr.ContentType, EPC_CT.EPC_EXTERNAL_PART_REFERENCE))
            trace_count_rawdata += DasExternalDatasetPart.Count // Raw.NumberOfLoci
        trace_count_rawdatatime = 0
        for DasExternalDatasetPart in Raw.RawDataTime.TimeArray.Values.ExternalFileProxy:
            eepr = DasExternalDatasetPart.EpcExternalPartReference
            if eepr.ContentType != EPC_CT.EPC_EXTERNAL_PART_REFERENCE:
                raise ValueError('Content Type error: {} is not equal to {}'.format(eepr.ContentType, EPC_CT.EPC_EXTERNAL_PART_REFERENCE))
            trace_count_rawdatatime += DasExternalDatasetPart.Count
        if trace_count_rawdatatime != trace_count_rawdata:
            raise ValueError('Number of traces is different for RawData ({}) and RawDatatime ({}) datasets: {}'.format(trace_count_rawdata, trace_count_rawdatatime, Raw.uuid))
        if Raw.RawDataTriggerTime is not None:
            for DasExternalDatasetPart in Raw.RawDataTriggerTime.TimeArray.Values.ExternalFileProxy:
                eepr = DasExternalDatasetPart.EpcExternalPartReference
                if eepr.ContentType != EPC_CT.EPC_EXTERNAL_PART_REFERENCE:
                    raise ValueError('Content Type error: {} is not equal to {}'.format(eepr.ContentType, EPC_CT.EPC_EXTERNAL_PART_REFERENCE))
    if das_acquisition.Processed:
        for Fbe in das_acquisition.Processed.Fbe:
            trace_count_fbedatatime = 0
            for DasExternalDatasetPart in Fbe.FbeDataTime.TimeArray.Values.ExternalFileProxy:
                eepr = DasExternalDatasetPart.EpcExternalPartReference
                if eepr.ContentType != EPC_CT.EPC_EXTERNAL_PART_REFERENCE:
                    raise ValueError('Content Type error: {} is not equal to {}'.format(eepr.ContentType, EPC_CT.EPC_EXTERNAL_PART_REFERENCE))
                trace_count_fbedatatime += DasExternalDatasetPart.Count
            for FbeData in Fbe.FbeData:
                trace_count_fbedata = 0
                for DasExternalDatasetPart in FbeData.FbeDataArray.Values.ExternalFileProxy:
                    eepr = DasExternalDatasetPart.EpcExternalPartReference
                    if eepr.ContentType != EPC_CT.EPC_EXTERNAL_PART_REFERENCE:
                        raise ValueError('Content Type error: {} is not equal to {}'.format(eepr.ContentType, EPC_CT.EPC_EXTERNAL_PART_REFERENCE))
                    trace_count_fbedata += DasExternalDatasetPart.Count // Fbe.NumberOfLoci
                if trace_count_fbedatatime != trace_count_fbedata:
                    raise ValueError('Number of traces is different for FbeData ({}) and FbeDatatime ({}) datasets: {}'.format(trace_count_fbedata, trace_count_fbedatatime, Fbe.uuid))
        for Spectra in das_acquisition.Processed.Spectra:
            for DasExternalDatasetPart in Spectra.SpectraData.SpectraDataArray.Values.ExternalFileProxy:
                eepr = DasExternalDatasetPart.EpcExternalPartReference
                if eepr.ContentType != EPC_CT.EPC_EXTERNAL_PART_REFERENCE:
                        raise ValueError('Content Type error: {} is not equal to {}'.format(eepr.ContentType, EPC_CT.EPC_EXTERNAL_PART_REFERENCE))
            for DasExternalDatasetPart in Spectra.SpectraDataTime.TimeArray.Values.ExternalFileProxy:
                eepr = DasExternalDatasetPart.EpcExternalPartReference
                if eepr.ContentType != EPC_CT.EPC_EXTERNAL_PART_REFERENCE:
                    raise ValueError('Content Type error: {} is not equal to {}'.format(eepr.ContentType, EPC_CT.EPC_EXTERNAL_PART_REFERENCE))

def validate_core_properties(xml_content):
    """Validate core properties."""
    if type(xml_content) == str:
        xml_content = xml_content.encode()
    core_properties = objectify.fromstring(xml_content)
    try:
        if not core_properties.version:
            raise ValueError('Core Properties should have non-empty version')
        if not core_properties['{http://purl.org/dc/elements/1.1/}creator']:
            raise ValueError('Core Properties should have non-empty dc:creator')
        if not core_properties['{http://purl.org/dc/terms/}created']:
            raise ValueError('Core Properties should have non-empty dcterms:created')
    except AttributeError as e:
        raise ValueError('There are missing attirbutes in Core Properties: {}'.format(e))

def validate_external_path(dirname, filename):
    """Raise an exception if external file does not exist."""
    if not os.path.exists(os.path.join(dirname, filename)):
        raise FileNotFoundError('External file {} does not exist'.format(filename))
    if os.path.splitext(filename)[1] not in {'.h5', '.H5'}:
        raise ValueError('HDF file extension is not .h5 or .H5: {}'.format(filename))

def validate_hdf_xml_values(hdf_value, xml_value, name, filename): #TODO expond docstring
    """Raise an exception if hdf value is not equal to xml value.

    Supports arrays of values (and boolean values through h5py).
    """
    # Function to check individual value.
    def check_value(hdf_item, xml_item):
        if isinstance(hdf_item, bytes):
            hdf_item = hdf_item.decode()
        if isinstance(hdf_item, numbers.Real):
            xml_item = float(xml_item)
            if not math.isclose(xml_item, hdf_item, rel_tol=TOLERANCE_FLOAT_REL, abs_tol=TOLERANCE_FLOAT_ABS):
                raise ValueError('{} is not equal in HDF ({}) and XML ({}): file {}'.format(name, hdf_value, xml_value, filename))
            return
        elif isinstance(hdf_item, numbers.Integral):
            xml_item = int(xml_item)
        else:
            hdf_item = str(hdf_item)
            xml_item = str(xml_item)
        if hdf_item != xml_item:
            raise ValueError('{} is not equal in HDF ({}) and XML ({}): file {}'.format(name, hdf_value, xml_value, filename))
    # End of subtoutines.
    if isinstance(xml_value, list):
        if not isinstance(hdf_value, np.ndarray):
            raise ValueError('{} is not equal in HDF ({}) and XML ({}): file {}'.format(name, hdf_value, xml_value, filename))
        if hdf_value.shape[0] != len(xml_value):
            raise ValueError('{} is not equal in HDF ({}) and XML ({}): file {}'.format(name, hdf_value, xml_value, filename))
        for i in range(len(hdf_value)):
            check_value(hdf_value[i], xml_value[i])
    else:
        check_value(hdf_value, xml_value)


def validate_hdf_attrs(f, path, attrs, filename):
    """Validate hdf file `f` group `path`.

    Use `attrs` as a dict of keys with array `[<required>, <type>]` or `[<required>, <type>, <value>]`, where first is True or False, second is one of the following: {'str', 'strarray', 'int', 'float', 'timestamp', 'bool'}, and optional third element is the corresponding value from XML metadata.  If third element is absent, then value equality is not checked.
    """
    for key in attrs:
        if attrs[key][0]:
            if key not in f[path].attrs:
                raise KeyError('Key "{}" is required in "{}" in "{}"'.format(key, path, filename))
        if key in f[path].attrs:
            if attrs[key][1] == 'str':
                if not isinstance(f[path].attrs[key], (bytes, str)):
                    raise TypeError('Attribute "{}" of group "{}" has wrong type "{}" (must be "{}") (in file {})'.format(key, path, type(f[path].attrs[key]), 'string', filename))
            elif attrs[key][1] == 'uuid':
                if not isinstance(f[path].attrs[key], (bytes, str)) or not is_valid_uuid(f[path].attrs[key]):
                    raise TypeError('Attribute "{}" of group "{}" has wrong type "{}" (must be "{}") (in file {})'.format(key, path, type(f[path].attrs[key]), 'uuid', filename))
            elif attrs[key][1] == 'strarray':
                if not isinstance(f[path].attrs[key], (np.ndarray)) and issubclass(f[path].attrs[key].dtype, (bytes, str)):
                    raise TypeError('Attribute "{}" of group "{}" has wrong type "{}" (must be "{}") (in file {})'.format(key, path, type(f[path].attrs[key]), 'string array', filename))
            elif attrs[key][1] == 'float':
                if not isinstance(f[path].attrs[key], (numbers.Real)):
                    raise TypeError('Attribute "{}" of group "{}" has wrong type "{}" (must be "{}") (in file {})'.format(key, path, type(f[path].attrs[key]), 'floating point number', filename))
            elif attrs[key][1] == 'int':
                if not isinstance(f[path].attrs[key], (numbers.Integral)):
                    raise TypeError('Attribute "{}" of group "{}" has wrong type "{}" (must be "{}") (in file {})'.format(key, path, type(f[path].attrs[key]), 'integer number', filename))
            elif attrs[key][1] == 'bool':
                if not isinstance(f[path].attrs[key], (np.bool_)):
                    raise TypeError('Attribute "{}" of group "{}" has wrong type "{}" (must be "{}") (in file {})'.format(key, path, type(f[path].attrs[key]), 'boolean', filename))
            elif attrs[key][1] == 'timestamp':
                if not isinstance(f[path].attrs[key], (bytes, str)):
                    raise TypeError('Attribute "{}" of group "{}" has wrong type "{}" (must be "{}") (in file {})'.format(key, path, type(f[path].attrs[key]), 'timestamp string (%Y-%m-%dT%H:%M:%S.%f%z)', filename))
                s = f[path].attrs[key]
                if isinstance(s, bytes):
                    s = s.decode()
                try:
                    parse_timestamp(s)
                except ValueError:
                    raise TypeError('Attribute "{}" of group "{}" has wrong type "{}" (must be "{}") (in file {})'.format(key, path, type(f[path].attrs[key]), 'timestamp string (%Y-%m-%dT%H:%M:%S.%f%z)', filename))
            else:
                raise KeyError('Type "{}" is not recognized.'.format(attrs[key][1]))
            if len(attrs[key]) == 3:
                validate_hdf_xml_values(f[path].attrs[key], attrs[key][2], key, filename)
    for key in f[path].attrs.keys():
        if key not in attrs:
            raise KeyError('Key "{}" is unknown in "{}" in "{}"'.format(key, path, filename))

def validate_hdf_file(dirname, filename, das_acquisition):
    """Raise an exception if attributes/datasets in HDF file are not valid.

    For most of objects only types are checed, while for '/Acquisition' and '/Acquisition/Calibration...' also values are checked for equality to XML values.
    """
    validate_external_path(dirname, filename)
    with h5py.File(os.path.join(dirname, filename), 'r') as f:
        # Root.
        if tuple(f.attrs.keys()) != ('uuid',):
            raise Exception('Wrong attribute of "/" group')
        if not isinstance(f.attrs['uuid'], (bytes, str)):
            raise TypeError('Attribute "uuid" of "/" group has wrong type {} (must be str)'.format(type(f.attrs['uuid'])))
        # Group and dataset attribute dicts (in format `key: [required, type]`).
        attrsAcquisition = {
            'AcquisitionDescription': [False, 'str', das_acquisition.AcquisitionDescription],
            'AcquisitionId': [True, 'str', das_acquisition.AcquisitionId],
            'FacilityId': [True, 'strarray', das_acquisition.FacilityId],
            'GaugeLength': [True, 'float', das_acquisition.GaugeLength.valueOf_],
            'GaugeLengthUnit': [True, 'str', das_acquisition.GaugeLength.uom],
            'MaximumFrequency': [True, 'float', das_acquisition.MaximumFrequency.valueOf_],
            'MeasurementStartTime': [True, 'timestamp', das_acquisition.MeasurementStartTime],
            'MinimumFrequency': [True, 'float', das_acquisition.MinimumFrequency.valueOf_],
            'NumberOfLoci': [True, 'int', das_acquisition.NumberOfLoci],
            'PulseRate': [True, 'float', das_acquisition.PulseRate.valueOf_],
            'PulseWidth': [True, 'float', das_acquisition.PulseWidth.valueOf_],
            'PulseWidthUnit': [True, 'str', das_acquisition.PulseWidth.uom],
            'schemaVersion': [True, 'str', das_acquisition.schemaVersion],
            'SpatialSamplingInterval': [True, 'float', das_acquisition.SpatialSamplingInterval.valueOf_],
            'SpatialSamplingIntervalUnit': [True, 'str', das_acquisition.SpatialSamplingInterval.uom],
            'StartLocusIndex': [True, 'int', das_acquisition.StartLocusIndex],
            'TriggeredMeasurement': [True, 'bool', das_acquisition.TriggeredMeasurement],
            'uuid': [True, 'str', das_acquisition.uuid],
            'VendorCode': [True, 'str', das_acquisition.VendorCode.Name]
        }
        # /Acquisition
        path = '/Acquisition'
        validate_hdf_attrs(f, path, attrsAcquisition, filename)
        # /Acquisition/Calibration[N] and Calibration data.
        ## Calibration data in the HDF file should be the same as calibration data in the xml file.
        Calibration = das_acquisition.Calibration
        count = 0
        for pathCalibration in f['/Acquisition'].keys():
            match = re.match('^Calibration\[(\d+)\]$', pathCalibration)
            if not match:
                continue
            pathCalibration = '/Acquisition/' + pathCalibration
            index = int(match.group(1))
            attrsCalibration = make_attrsCalibration()
            attrsCalibration['CalibrationDatum'].append(Calibration[index].CalibrationDatum)
            attrsCalibration['CalibrationDescription'].append(Calibration[index].CalibrationDescription)
            attrsCalibration['CalibrationFacilityLengthUnit'].append(Calibration[index].CalibrationFacilityLengthUnit)
            attrsCalibration['CalibrationIndex'].append(Calibration[index].CalibrationIndex)
            attrsCalibration['CalibrationOpticalPathDistanceUnit'].append(Calibration[index].CalibrationOpticalPathDistanceUnit)
            attrsCalibration['FacilityKind'].append(Calibration[index].FacilityKind)
            attrsCalibration['FacilityName'].append(Calibration[index].FacilityName)
            attrsCalibration['NumberOfCalibrationPoints'].append(Calibration[index].NumberOfCalibrationPoints)
            validate_hdf_attrs(f, pathCalibration, attrsCalibration, filename)
            pathCDP = pathCalibration + '/CalibrationDataPoints'
            if f[pathCDP].shape[0] != f[pathCalibration].attrs['NumberOfCalibrationPoints']:
                raise ValueError('NumberOfCalibrationPoints ({}) is not equal to dataset size ({}) in HDF: in file {}'.format(
                    f[pathCalibration].attrs['NumberOfCalibrationPoints'], f[pathCDP].shape[0], filename
                ))
            for j, cdp in enumerate(Calibration[index].CalibrationDataPoints):
                validate_hdf_xml_values(f[pathCDP][j, 'CalibrationLocusIndex'], cdp.CalibrationLocusIndex, 'CalibrationLocusIndex', filename)
                validate_hdf_xml_values(f[pathCDP][j, 'CalibrationOpticalPathDistance'], cdp.CalibrationOpticalPathDistance.valueOf_, 'CalibrationOpticalPathDistance', filename)
                validate_hdf_xml_values(f[pathCDP][j, 'CalibrationFacilityLength'], cdp.CalibrationFacilityLength.valueOf_, 'CalibrationFacilityLength', filename)
                validate_hdf_xml_values(f[pathCDP][j, 'CalibrationType'], cdp.CalibrationType, 'CalibrationType', filename)
            count += 1
            if count > len(Calibration):
                raise ValueError('There are more Calibration objects in the HDF file ({}) than in the XML file ({}): in file {}'.format(count, len(Calibration), filename))
        if count < len(Calibration):
            raise ValueError('There are fewer Calibration objects in the HDF file ({}) than in the XML file ({}): in file {}'.format(count, len(Calibration), filename))

def validate_raw(Raw, pathRaw, hdf_file, filename):
    """Validate HDF Raw group.
    """
    attrsRaw = make_attrsRaw()
    attrsRaw['RawDataUnit'].append(Raw.RawDataUnit)
    attrsRaw['NumberOfLoci'].append(Raw.NumberOfLoci)
    attrsRaw['StartLocusIndex'].append(Raw.StartLocusIndex)
    attrsRaw['uuid'].append(Raw.uuid)
    attrsRaw['OutputDataRate'].append(Raw.OutputDataRate.valueOf_)
    attrsRaw['RawDescription'].append(Raw.RawDescription)
    attrsRaw['RawIndex'].append(Raw.RawIndex)
    validate_hdf_attrs(hdf_file, pathRaw, attrsRaw, filename)

def validate_rawdata(Raw, DasExternalDatasetPart, filename):
    """Validate HDF RawData dataset.
    """
    with h5py.File(filename, 'r') as f:
        # Validate dataset path.
        pathRawData = DasExternalDatasetPart.PathInExternalFile
        if not re.match('^/Acquisition/Raw(\[\d+\])?/RawData$', pathRawData):
            raise ValueError('Wrong group name for RawData: {} (must be /Raw/RawData or /Raw[N]/RawData)'.format(pathRawData))
        if pathRawData not in f:
            raise ValueError('{} does not contain the {} dataset'.format(filename, pathRawData))
        # Validate parent group.
        pathRaw = parent_hdf_path(pathRawData)
        validate_raw(Raw, pathRaw, f, filename)
        # Validate dataset attributes.
        attrsRawData = make_attrsData()
        attrsRawData['Count'].append(DasExternalDatasetPart.Count)
        attrsRawData['Dimensions'].append(Raw.RawData.Dimensions)
        attrsRawData['PartStartTime'].append(DasExternalDatasetPart.PartStartTime)
        attrsRawData['PartEndTime'].append(DasExternalDatasetPart.PartEndTime)
        attrsRawData['StartIndex'].append(DasExternalDatasetPart.StartIndex)
        validate_hdf_attrs(f, pathRawData, attrsRawData, filename)
        # Validate dataset shape.
        if f[pathRawData].shape[Raw.RawData.Dimensions.index('locus')] != Raw.NumberOfLoci:
            raise ValueError('Size of locus dimension in HDF dataset ({}) is not equal to NumberOfLoci value in XML metadata ({}): in {}'.format(f[pathRawData].shape[Raw.RawData.Dimensions.index('locus')], Raw.NumberOfLoci, filename))
        if reduce(operator.mul, f[pathRawData].shape) != DasExternalDatasetPart.Count:
            raise ValueError('Size of HDF dataset ({}) is not equal to Count value in XML metadata ({}): in {}'.format(operator.mul(f[pathRawData].shape), DasExternalDatasetPart.Count, filename))

def validate_rawdatatime(Raw, DasExternalDatasetPart, filename):
    """Validate HDF RawDataTime dataset.
    """
    with h5py.File(filename, 'r') as f:
        # Validate dataset path.
        pathRawDataTime = DasExternalDatasetPart.PathInExternalFile
        if not re.match('^/Acquisition/Raw(\[\d+\])?/RawDataTime$', pathRawDataTime):
            raise ValueError('Wrong group name for RawDataTime: {} (must be /Raw/RawDataTime or /Raw[N]/RawDataTime)'.format(pathRawDataTime))
        if pathRawDataTime not in f:
            raise ValueError('{} does not contain the {} dataset'.format(filename, pathRawDataTime))
        # Validate parent group.
        pathRaw = parent_hdf_path(pathRawDataTime)
        validate_raw(Raw, pathRaw, f, filename)
        # Validate dataset attributes.
        attrsDataTime = make_attrsDataTime()
        attrsDataTime['Count'].append(DasExternalDatasetPart.Count)
        attrsDataTime['PartStartTime'].append(DasExternalDatasetPart.PartStartTime)
        attrsDataTime['PartEndTime'].append(DasExternalDatasetPart.PartEndTime)
        attrsDataTime['StartTime'].append(Raw.RawDataTime.StartTime)
        attrsDataTime['EndTime'].append(Raw.RawDataTime.EndTime)
        attrsDataTime['StartIndex'].append(DasExternalDatasetPart.StartIndex)
        validate_hdf_attrs(f, pathRawDataTime, attrsDataTime, filename)
        # Validate dataset shape.
        if reduce(operator.mul, f[pathRawDataTime].shape) != DasExternalDatasetPart.Count:
            raise ValueError('Size of HDF dataset ({}) is not equal to Count value in XML metadata ({}): in {}'.format(operator.mul(f[pathRawDataTime].shape), DasExternalDatasetPart.Count, filename))
        # Validate dataset values.
        if np.any(np.absolute(np.diff(f[pathRawDataTime], 2)) > TOLERANCE_TIME_ABS):
            raise ValueError('HDF dataset ({}) with time has different difference step between neighbouring elements: in {}'.format(pathRawDataTime, filename))
        # Compare first and last timestamps in the array to timestamps in metadata.
        start_time_data = dt.datetime.fromtimestamp(f[pathRawDataTime][0]//1000000, dt.timezone.utc)+dt.timedelta(microseconds=int(f[pathRawDataTime][0]%1000000))
        start_time_attr = parse_timestamp(DasExternalDatasetPart.PartStartTime)
        if start_time_data != start_time_attr:
            raise ValueError('HDF dataset start time ({}) is different from HDF metadata start time ({})'.format(start_time_data, start_time_attr))
        end_time_data = dt.datetime.fromtimestamp(f[pathRawDataTime][-1]//1000000, dt.timezone.utc)+dt.timedelta(microseconds=int(f[pathRawDataTime][-1]%1000000))
        end_time_attr = parse_timestamp(DasExternalDatasetPart.PartEndTime)
        if start_time_data != start_time_attr:
            raise ValueError('HDF dataset start time ({}) is different from HDF metadata start time ({})'.format(start_time_data, start_time_attr))

def validate_rawdatatriggertime(Raw, DasExternalDatasetPart, filename):
    """Validate HDF RawDataTriggerTime dataset.
    """
    with h5py.File(filename, 'r') as f:
        # Validate dataset path.
        pathRawDataTriggerTime = DasExternalDatasetPart.PathInExternalFile
        if not re.match('^/Acquisition/Raw(\[\d+\])?/RawDataTriggerTime$', pathRawDataTriggerTime):
            raise ValueError('Wrong group name for RawDataTriggerTime: {} (must be /Raw/RawDataTriggerTime or /Raw[N]/RawDataTriggerTime)'.format(pathRawDataTriggerTime))
        if pathRawDataTriggerTime not in f:
            raise ValueError('{} does not contain the {} dataset'.format(filename, pathRawDataTriggerTime))
        # Validate parent group.
        pathRaw = parent_hdf_path(pathRawDataTriggerTime)
        validate_raw(Raw, pathRaw, f, filename)
        # Validate dataset attributes.
        attrsDataTime = make_attrsDataTime()
        attrsDataTime['Count'].append(DasExternalDatasetPart.Count)
        attrsDataTime['PartStartTime'].append(DasExternalDatasetPart.PartStartTime)
        attrsDataTime['PartEndTime'].append(DasExternalDatasetPart.PartEndTime)
        attrsDataTime['StartTime'].append(Raw.RawDataTriggerTime.StartTime)
        attrsDataTime['EndTime'].append(Raw.RawDataTriggerTime.EndTime)
        attrsDataTime['StartIndex'].append(DasExternalDatasetPart.StartIndex)
        validate_hdf_attrs(f, pathRawDataTriggerTime, attrsDataTime, filename)
        # Validate dataset shape.
        if reduce(operator.mul, f[pathRawDataTriggerTime].shape) != DasExternalDatasetPart.Count:
            raise ValueError('Size of HDF dataset ({}) is not equal to Count value in XML metadata ({}): in {}'.format(operator.mul(f[pathRawDataTriggerTime].shape), DasExternalDatasetPart.Count, filename))

def validate_fbe(Fbe, pathFbe, hdf_file, filename):
    """Validate HDF Raw group.
    """
    attrsFbe = make_attrsFbe()
    attrsFbe['FbeDataUnit'].append(Fbe.FbeDataUnit)
    attrsFbe['OutputDataRate'].append(Fbe.OutputDataRate.valueOf_)
    attrsFbe['StartLocusIndex'].append(Fbe.StartLocusIndex)
    attrsFbe['NumberOfLoci'].append(Fbe.NumberOfLoci)
    attrsFbe['uuid'].append(Fbe.uuid)
    attrsFbe['FbeIndex'].append(Fbe.FbeIndex)
    attrsFbe['FbeDescription'].append(Fbe.FbeDescription)
    attrsFbe['FilterType'].append(Fbe.FilterType)
    attrsFbe['SpatialSamplingInterval'].append(Fbe.SpatialSamplingInterval.valueOf_)
    attrsFbe['SpatialSamplingIntervalUnit'].append(Fbe.SpatialSamplingInterval.uom)
    attrsFbe['SpectraReference'].append(Fbe.SpectraReference)
    attrsFbe['TransformType'].append(Fbe.TransformType)
    attrsFbe['TransformSize'].append(Fbe.TransformSize)
    attrsFbe['RawReference'].append(Fbe.RawReference)
    attrsFbe['WindowFunction'].append(Fbe.WindowFunction)
    attrsFbe['WindowSize'].append(Fbe.WindowSize)
    attrsFbe['WindowOverlap'].append(Fbe.WindowOverlap)
    validate_hdf_attrs(hdf_file, pathFbe, attrsFbe, filename)

def validate_fbedata(Fbe, FbeData, DasExternalDatasetPart, filename):
    """Validate HDF FbeData dataset.
    """
    with h5py.File(filename, 'r') as f:
        # Validate dataset path.
        pathFbeData = DasExternalDatasetPart.PathInExternalFile
        if not re.match('^/Acquisition/Processed/Fbe(\[\d+\])?/FbeData(\[\d+\])?$', pathFbeData):
            raise ValueError('Wrong group name for FbeData: {} (must be /Processed/Fbe[N]/FbeData[M]; [N] and [M] parts are optional)'.format(pathFbeData))
        if pathFbeData not in f:
            raise ValueError('{} does not contain the {} dataset'.format(filename, pathFbeData))
        # Validate parent group.
        pathFbe = parent_hdf_path(pathFbeData)
        validate_fbe(Fbe, pathFbe, f, filename)
        # Validate dataset attributes.
        attrsProcessedData = make_attrsDataProcessed()
        attrsProcessedData['Count'].append(DasExternalDatasetPart.Count)
        attrsProcessedData['Dimensions'].append(FbeData.Dimensions)
        attrsProcessedData['EndFrequency'].append(FbeData.EndFrequency.valueOf_)
        attrsProcessedData['PartStartTime'].append(DasExternalDatasetPart.PartStartTime)
        attrsProcessedData['PartEndTime'].append(DasExternalDatasetPart.PartEndTime)
        attrsProcessedData['StartFrequency'].append(FbeData.StartFrequency.valueOf_)
        attrsProcessedData['StartIndex'].append(DasExternalDatasetPart.StartIndex)
        validate_hdf_attrs(f, pathFbeData, attrsProcessedData, filename)
        # Validate dataset shape.
        if f[pathFbeData].shape[FbeData.Dimensions.index('locus')] != Fbe.NumberOfLoci:
            raise ValueError('Size of locus dimension in HDF dataset ({}) is not equal to NumberOfLoci value in XML metadata ({}): in {}'.format(f[pathFbeData].shape[FbeData.Dimensions.index('locus')], Fbe.NumberOfLoci, filename))
        if reduce(operator.mul, f[pathFbeData].shape) != DasExternalDatasetPart.Count:
            raise ValueError('Size of HDF dataset ({}) is not equal to Count value in XML metadata ({}): in {}'.format(operator.mul(f[pathFbeData].shape), DasExternalDatasetPart.Count, filename))

def validate_fbedatatime(Fbe, DasExternalDatasetPart, filename):
    """Validate HDF FbeDataTime dataset.
    """
    with h5py.File(filename, 'r') as f:
        # Validate dataset path.
        pathFbeDataTime = DasExternalDatasetPart.PathInExternalFile
        if not re.match('^/Acquisition/Processed/Fbe(\[\d+\])?/FbeDataTime$', pathFbeDataTime):
            raise ValueError('Wrong group name for FbeDataTime: {} (must be /Processed/Fbe/FbeDataTime or /Processed/Fbe[N]/FbeDataTime)'.format(pathFbeDataTime))
        if pathFbeDataTime not in f:
            raise ValueError('{} does not contain the {} dataset'.format(filename, pathFbeDataTime))
        # Validate parent group.
        pathFbe = parent_hdf_path(pathFbeDataTime)
        validate_fbe(Fbe, pathFbe, f, filename)
        # Validate dataset attributes.
        attrsDataTime = make_attrsDataTime()
        attrsDataTime['Count'].append(DasExternalDatasetPart.Count)
        attrsDataTime['PartStartTime'].append(DasExternalDatasetPart.PartStartTime)
        attrsDataTime['PartEndTime'].append(DasExternalDatasetPart.PartEndTime)
        attrsDataTime['StartTime'].append(Fbe.FbeDataTime.StartTime)
        attrsDataTime['EndTime'].append(Fbe.FbeDataTime.EndTime)
        attrsDataTime['StartIndex'].append(DasExternalDatasetPart.StartIndex)
        validate_hdf_attrs(f, pathFbeDataTime, attrsDataTime, filename)
        # Validate dataset shape.
        if reduce(operator.mul, f[pathFbeDataTime].shape) != DasExternalDatasetPart.Count:
            raise ValueError('Size of HDF dataset ({}) is not equal to Count value in XML metadata ({}): in {}'.format(operator.mul(f[pathFbeDataTime].shape), DasExternalDatasetPart.Count, filename))
        # Validate dataset values.
        if np.any(np.absolute(np.diff(f[pathFbeDataTime], 2)) > TOLERANCE_TIME_ABS):
            raise ValueError('HDF dataset ({}) with time has different difference step between neighbouring elements: in {}'.format(pathFbeDataTime, filename))
        # Compare first and last timestamps in the array to timestamps in metadata.
        start_time_data = dt.datetime.fromtimestamp(f[pathFbeDataTime][0]//1000000, dt.timezone.utc)+dt.timedelta(microseconds=int(f[pathFbeDataTime][0]%1000000))
        start_time_attr = parse_timestamp(DasExternalDatasetPart.PartStartTime)
        if start_time_data != start_time_attr:
            raise ValueError('HDF dataset start time ({}) is different from HDF metadata start time ({})'.format(start_time_data, start_time_attr))
        end_time_data = dt.datetime.fromtimestamp(f[pathFbeDataTime][-1]//1000000, dt.timezone.utc)+dt.timedelta(microseconds=int(f[pathFbeDataTime][-1]%1000000))
        end_time_attr = parse_timestamp(DasExternalDatasetPart.PartEndTime)
        if start_time_data != start_time_attr:
            raise ValueError('HDF dataset start time ({}) is different from HDF metadata start time ({})'.format(start_time_data, start_time_attr))

def validate_spectra(Spectra, pathSpectra, hdf_file, filename):
    """Validate HDF Spectra group.
    """
    attrsSpectra = make_attrsSpectra()
    attrsSpectra['SpectraDataUnit'].append(Spectra.SpectraDataUnit)
    attrsSpectra['OutputDataRate'].append(Spectra.OutputDataRate.valueOf_)
    attrsSpectra['StartLocusIndex'].append(Spectra.StartLocusIndex)
    attrsSpectra['NumberOfLoci'].append(Spectra.NumberOfLoci)
    attrsSpectra['TransformType'].append(Spectra.TransformType)
    attrsSpectra['TransformSize'].append(Spectra.TransformSize)
    attrsSpectra['uuid'].append(Spectra.uuid)
    attrsSpectra['SpectraIndex'].append(Spectra.SpectraIndex)
    attrsSpectra['SpectraDescription'].append(Spectra.SpectraDescription)
    attrsSpectra['FilterType'].append(Spectra.FilterType)
    attrsSpectra['SpatialSamplingInterval'].append(Spectra.SpatialSamplingInterval.valueOf_)
    attrsSpectra['SpatialSamplingIntervalUnit'].append(Spectra.SpatialSamplingInterval.uom)
    attrsSpectra['FbeReference'].append(Spectra.FbeReference)
    attrsSpectra['RawReference'].append(Spectra.RawReference)
    attrsSpectra['WindowFunction'].append(Spectra.WindowFunction)
    attrsSpectra['WindowSize'].append(Spectra.WindowSize)
    attrsSpectra['WindowOverlap'].append(Spectra.WindowOverlap)
    validate_hdf_attrs(hdf_file, pathSpectra, attrsSpectra, filename)

def validate_spectradata(Spectra, DasExternalDatasetPart, filename):
    """Validate HDF SpectraData dataset.
    """
    with h5py.File(filename, 'r') as f:
        # Validate dataset path.
        pathSpectraData = DasExternalDatasetPart.PathInExternalFile
        if not re.match('^/Acquisition/Processed/Spectra(\[\d+\])?/SpectraData$', pathSpectraData):
            raise ValueError('Wrong group name for SpectraData: {} (must be /Processed/Spectra[N]/SpectraData; [N] part is optional)'.format(pathSpectraData))
        if pathSpectraData not in f:
            raise ValueError('{} does not contain the {} dataset'.format(filename, pathSpectraData))
        # Validate parent group.
        pathSpectra = parent_hdf_path(pathSpectraData)
        validate_spectra(Spectra, pathSpectra, f, filename)
        # Validate dataset attributes.
        attrsProcessedData = make_attrsDataProcessed()
        attrsProcessedData['Count'].append(DasExternalDatasetPart.Count)
        attrsProcessedData['Dimensions'].append(Spectra.SpectraData.Dimensions)
        attrsProcessedData['EndFrequency'].append(Spectra.SpectraData.EndFrequency.valueOf_)
        attrsProcessedData['PartStartTime'].append(DasExternalDatasetPart.PartStartTime)
        attrsProcessedData['PartEndTime'].append(DasExternalDatasetPart.PartEndTime)
        attrsProcessedData['StartFrequency'].append(Spectra.SpectraData.StartFrequency.valueOf_)
        attrsProcessedData['StartIndex'].append(DasExternalDatasetPart.StartIndex)
        validate_hdf_attrs(f, pathSpectraData, attrsProcessedData, filename)
        # Validate dataset shape.
        if f[pathSpectraData].shape[Spectra.SpectraData.Dimensions.index('locus')] != Spectra.NumberOfLoci:
            raise ValueError('Size of locus dimension in HDF dataset ({}) is not equal to NumberOfLoci value in XML metadata ({}): in {}'.format(f[pathSpectraData].shape[Spectra.SpectraData.Dimensions.index('locus')], Spectra.NumberOfLoci, filename))
        if reduce(operator.mul, f[pathSpectraData].shape) != DasExternalDatasetPart.Count:
            raise ValueError('Size of HDF dataset ({}) is not equal to Count value in XML metadata ({}): in {}'.format(operator.mul(f[pathSpectraData].shape), DasExternalDatasetPart.Count, filename))

def validate_spectradatatime(Spectra, DasExternalDatasetPart, filename):
    """Validate HDF SpectraDataTime dataset.
    """
    with h5py.File(filename, 'r') as f:
        # Validate dataset path.
        pathSpectraDataTime = DasExternalDatasetPart.PathInExternalFile
        if not re.match('^/Acquisition/Processed/Spectra(\[\d+\])?/SpectraDataTime$', pathSpectraDataTime):
            raise ValueError('Wrong group name for SpectraDataTime: {} (must be /Processed/Spectra/SpectraDataTime or /Processed/Spectra[N]/SpectraDataTime)'.format(pathSpectraDataTime))
        if pathSpectraDataTime not in f:
            raise ValueError('{} does not contain the {} dataset'.format(filename, pathSpectraDataTime))
        # Validate parent group.
        pathSpectra = parent_hdf_path(pathSpectraDataTime)
        validate_spectra(Spectra, pathSpectra, f, filename)
        # Validate dataset attributes.
        attrsDataTime = make_attrsDataTime()
        attrsDataTime['Count'].append(DasExternalDatasetPart.Count)
        attrsDataTime['PartStartTime'].append(DasExternalDatasetPart.PartStartTime)
        attrsDataTime['PartEndTime'].append(DasExternalDatasetPart.PartEndTime)
        attrsDataTime['StartTime'].append(Spectra.SpectraDataTime.StartTime)
        attrsDataTime['EndTime'].append(Spectra.SpectraDataTime.EndTime)
        attrsDataTime['StartIndex'].append(DasExternalDatasetPart.StartIndex)
        validate_hdf_attrs(f, pathSpectraDataTime, attrsDataTime, filename)
        # Validate dataset shape.
        if reduce(operator.mul, f[pathSpectraDataTime].shape) != DasExternalDatasetPart.Count:
            raise ValueError('Size of HDF dataset ({}) is not equal to Count value in XML metadata ({}): in {}'.format(operator.mul(f[pathSpectraDataTime].shape), DasExternalDatasetPart.Count, filename))
        # Validate dataset values.
        if np.any(np.absolute(np.diff(f[pathSpectraDataTime], 2)) > TOLERANCE_TIME_ABS):
            raise ValueError('HDF dataset ({}) with time has different difference step between neighbouring elements: in {}'.format(pathSpectraDataTime, filename))
        # Compare first and last timestamps in the array to timestamps in metadata.
        start_time_data = dt.datetime.fromtimestamp(f[pathSpectraDataTime][0]//1000000, dt.timezone.utc)+dt.timedelta(microseconds=int(f[pathSpectraDataTime][0]%1000000))
        start_time_attr = parse_timestamp(DasExternalDatasetPart.PartStartTime)
        if start_time_data != start_time_attr:
            raise ValueError('HDF dataset start time ({}) is different from HDF metadata start time ({})'.format(start_time_data, start_time_attr))
        end_time_data = dt.datetime.fromtimestamp(f[pathSpectraDataTime][-1]//1000000, dt.timezone.utc)+dt.timedelta(microseconds=int(f[pathSpectraDataTime][-1]%1000000))
        end_time_attr = parse_timestamp(DasExternalDatasetPart.PartEndTime)
        if start_time_data != start_time_attr:
            raise ValueError('HDF dataset start time ({}) is different from HDF metadata start time ({})'.format(start_time_data, start_time_attr))

def validate_external_objects(package):
    """Validate all external references for Raw, Fbe and Spectra in a package.
    """
    for Raw in package.das_acquisition.Raw:
        for DasExternalDatasetPart in Raw.RawData.RawDataArray.Values.ExternalFileProxy:
            eepr = DasExternalDatasetPart.EpcExternalPartReference
            filename = package.get_full_hdf_path(eepr.Uuid)
            validate_rawdata(Raw, DasExternalDatasetPart, filename)
        for DasExternalDatasetPart in Raw.RawDataTime.TimeArray.Values.ExternalFileProxy:
            eepr = DasExternalDatasetPart.EpcExternalPartReference
            filename = package.get_full_hdf_path(eepr.Uuid)
            validate_rawdatatime(Raw, DasExternalDatasetPart, filename)
        if Raw.RawDataTriggerTime is not None:
            for DasExternalDatasetPart in Raw.RawDataTriggerTime.TimeArray.Values.ExternalFileProxy:
                eepr = DasExternalDatasetPart.EpcExternalPartReference
                filename = package.get_full_hdf_path(eepr.Uuid)
                validate_rawdatatriggertime(Raw, DasExternalDatasetPart, filename)
    if package.das_acquisition.Processed:
        for Fbe in package.das_acquisition.Processed.Fbe:
            for FbeData in Fbe.FbeData:
                for DasExternalDatasetPart in FbeData.FbeDataArray.Values.ExternalFileProxy:
                    eepr = DasExternalDatasetPart.EpcExternalPartReference
                    filename = package.get_full_hdf_path(eepr.Uuid)
                    validate_fbedata(Fbe, FbeData, DasExternalDatasetPart, filename)
            for DasExternalDatasetPart in Fbe.FbeDataTime.TimeArray.Values.ExternalFileProxy:
                eepr = DasExternalDatasetPart.EpcExternalPartReference
                filename = package.get_full_hdf_path(eepr.Uuid)
                validate_fbedatatime(Fbe, DasExternalDatasetPart, filename)
        for Spectra in package.das_acquisition.Processed.Spectra:
            for DasExternalDatasetPart in Spectra.SpectraData.SpectraDataArray.Values.ExternalFileProxy:
                eepr = DasExternalDatasetPart.EpcExternalPartReference
                filename = package.get_full_hdf_path(eepr.Uuid)
                validate_spectradata(Spectra, DasExternalDatasetPart, filename)
            for DasExternalDatasetPart in Spectra.SpectraDataTime.TimeArray.Values.ExternalFileProxy:
                eepr = DasExternalDatasetPart.EpcExternalPartReference
                filename = package.get_full_hdf_path(eepr.Uuid)
                validate_spectradatatime(Spectra, DasExternalDatasetPart, filename)
