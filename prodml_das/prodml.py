# License notice
#
# Copyright 2017
#
# Energistics
# The following Energistics (c) products were used in the creation of this work:
#
# •             PRODML Data Schema Specifications, Version 1.3.1
# •             PRODML Data Schema Specifications, Version 2.0
#
# All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
# any portion thereof, which remain in the Standards DevKit shall remain with Energistics
# or its suppliers and shall remain subject to the terms of the Product License Agreement
# available at http://www.energistics.org/product-license-agreement.
#
# Apache
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
# except in compliance with the License.
#
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the
# License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the License for the specific language governing permissions and limitations under the
# License.
#
# All rights reserved.
#

import datetime
import io
import logging
import os
import uuid

from lxml import etree
from lxml import objectify
import h5py
import numpy as np
import opc
from opc.constants import CONTENT_TYPE as CT
from opc.constants import RELATIONSHIP_TYPE as RT
from opc.packuri import PackURI


import prodml_das.DasAcquisition as da

import prodml_das.gds_adapter as ga

from prodml_das.constants import EPC_CT, EPC_RT, PRODML_SCHEMA
import prodml_das.validation as validation

from prodml_das.utils import parent_hdf_path

#=============== Constants

EPC_EXTENSION = '.epc'
EPC_EXTENSION_WARNING = 'The EPC package shall have the ".epc" file extension!'

VALIDATION_LEVEL_NONE = 0
VALIDATION_LEVEL_XML_VALIDITY = 10
VALIDATION_LEVEL_XML_CONTENT = 15
VALIDATION_LEVEL_EXTERNAL_PRESENCE = 20
VALIDATION_LEVEL_EXTERNAL_CONTENT = 30

#=============== PMLproxy class

class CoreProperties(object):
    """Core properties.
    """

    def __init__(self, creator='Test', version='1.0', created=None, xml=None):
        """Initialize core properties.
        """
        if xml is not None:
            self.load_from_xml(xml)
            return
        self.creator = creator
        self.version = version
        if created is None:
            self.created = datetime.datetime.utcnow().isoformat()+'+00:00'
        else:
            self.created = created

    def make_xml(self):
        """Return XML representation of the core properties.
        """
        nsmap = {'cp': 'http://schemas.openxmlformats.org/package/2006/metadata/core-properties',
                 'dc': 'http://purl.org/dc/elements/1.1/',
                 'dcterms': 'http://purl.org/dc/terms/',
                 'dcmitype': 'http://purl.org/dc/dcmitype/',
                 'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}
        core_properties = etree.Element("{http://schemas.openxmlformats.org/package/2006/metadata/core-properties}coreProperties", nsmap=nsmap)
        creator = etree.SubElement(core_properties, '{http://purl.org/dc/elements/1.1/}creator')
        creator.text = str(self.creator)
        version = etree.SubElement(core_properties, '{http://schemas.openxmlformats.org/package/2006/metadata/core-properties}version')
        version.text = str(self.version)
        created = etree.SubElement(core_properties, '{http://purl.org/dc/terms/}created')
        created.text = str(self.created)
        created.set('{http://www.w3.org/2001/XMLSchema-instance}type', 'dcterms:W3CDTF')
        return etree.tostring(core_properties, pretty_print=True)

    def load_from_xml(self, blob):
        """Load from xml string.
        """
        cp_obj = objectify.fromstring(xml_content)
        self.version = cp_obj.creator
        self.creator = core_properties['{http://purl.org/dc/elements/1.1/}creator']
        self.created = core_properties['{http://purl.org/dc/terms/}created']

def load_eepr(blob):
    """Loads EpcExternalPartReference from xml.
    """
    if isinstance(blob, str):
        blob = blob.encode()
    blob_input = io.BytesIO(blob)
    eepr = ga.parse(blob_input, silence=True)
    blob_input.close()
    return eepr

class PMLproxy(object):

    '''ProdML proxy object
    '''

    def __init__(self, path, exist_ok=True, validation_level=VALIDATION_LEVEL_XML_CONTENT, raw_dtype=np.uint16, fbe_dtype=np.float32, spectra_dtype=np.float32, time_dtype=np.int64):
        """Open or create a new EPC file.

        Create an EPC package, and if exist_ok is False, then raise a ValueError if file already exists, otherwise open the existing file.

        validation_level defines how much validation is done:
         * level 1 checks that XML data and package structure;
         * level 2 checks that external .h5 files are present;
         * level 3 checks contents of HDF files.
        """
        self.epc_path = path
        if not path.endswith(EPC_EXTENSION):
            logging.warning(EPC_EXTENSION_WARNING)
        self.epc_folder = os.path.dirname(path)
        self.package = None
        self.das_acquisition = None
        self.das_instrument_box = None
        self.fiber_optical_path = None
        self.das_acquisition_part = None
        self.das_instrument_box_part = None
        self.fiber_optical_path_part = None
        self.raw_dtype = raw_dtype
        self.fbe_dtype = fbe_dtype
        self.spectra_dtype = spectra_dtype
        self.time_dtype = time_dtype
        self.eeprs = {}
        # Open file if exists, otherwise create a new one.
        if os.path.exists(self.epc_path):
            if exist_ok:
                self.open(validation_level)
            else:
                raise ValueError('File already exists: {}'.format(path))
        else:
            self.create()

    def open(self, validation_level=VALIDATION_LEVEL_XML_CONTENT):
        """Open existing file.
        """
        self.package = opc.OpcPackage.open(self.epc_path)
        for part in self.package.all_parts:
            if part._content_type == CT.OPC_CORE_PROPERTIES:
                if validation_level >= VALIDATION_LEVEL_XML_VALIDITY:
                    validation.validate_core_properties(part._blob)
                self.core_properties = CoreProperties()
                self.core_properties_part = part
            elif part._content_type == EPC_CT.DAS_ACQUISITION:
                if validation_level >= VALIDATION_LEVEL_XML_VALIDITY:
                    validation.validate_xml(part._blob, PRODML_SCHEMA.DAS_ACQUISITION)
                self.das_acquisition = ga.parse(io.BytesIO(part._blob), silence=True)
                if validation_level >= VALIDATION_LEVEL_XML_CONTENT:
                    validation.validate_das_acquisition(self.das_acquisition)
                self.das_acquisition_part = part
                for rel in part.rels:
                    if rel._reltype == EPC_RT.EXTERNAL_PART_PROXY_TO_ML:
                        for eepr_rel in rel.target_part.rels:
                            eepr = load_eepr(rel.target_part.blob)
                            if eepr_rel._reltype == EPC_RT.EXTERNAL_RESOURCE:
                                self.eeprs[eepr.uuid] = eepr
                                if validation_level >= VALIDATION_LEVEL_EXTERNAL_CONTENT:
                                    validation.validate_hdf_file(self.epc_folder, eepr_rel.target_ref, self.das_acquisition)
            elif part._content_type == EPC_CT.DAS_INSTRUMENT_BOX:
                if validation_level >= VALIDATION_LEVEL_XML_VALIDITY:
                    validation.validate_xml(part._blob, PRODML_SCHEMA.DAS_ACQUISITION)
                self.das_instrument_box = ga.parse(io.BytesIO(part._blob), silence=True)
                self.das_instrument_box_part = part
            elif part._content_type == EPC_CT.FIBER_OPTICAL_PATH:
                if validation_level >= VALIDATION_LEVEL_XML_VALIDITY:
                    validation.validate_xml(part._blob, PRODML_SCHEMA.FIBER_OPTICAL_PATH)
                self.fiber_optical_path = ga.parse(io.BytesIO(part._blob), silence=True)
                self.fiber_optical_path_part = part
            elif part._content_type == EPC_CT.EPC_EXTERNAL_PART_REFERENCE:
                if validation_level >= VALIDATION_LEVEL_XML_VALIDITY:
                    validation.validate_xml(part._blob, PRODML_SCHEMA.EPC_EXTERNAL_PART_REFERENCE)
                for rel in part.rels:
                    if rel._reltype == EPC_RT.EXTERNAL_RESOURCE:
                        if validation_level >= VALIDATION_LEVEL_EXTERNAL_PRESENCE:
                            validation.validate_external_path(self.epc_folder, rel.target_ref)
        try:
            self.package.rels.get_rel_of_type(RT.CORE_PROPERTIES)
        except KeyError as e:
            raise KeyError('No core propertes relationship found in the EPC package')
        except ValueError as e:
            raise ValueError('Multiple core propertes relationships found in the EPC package')
        if self.das_acquisition is None:
            raise ValueError('No DasAcquisition is found in the EPC package')
        if self.fiber_optical_path is None:
            raise ValueError('No FiberOpticalPath is found in the EPC package')
        if self.das_instrument_box is None:
            raise ValueError('No DasInstrumentBox is found in the EPC package')
        if validation_level >= VALIDATION_LEVEL_EXTERNAL_CONTENT:
            validation.validate_external_objects(self)

    def create(self):
        """Create a new file.
        """
        self.package = opc.OpcPackage()
        # Objects.
        self.das_acquisition = da.DasAcquisition.factory()
        self.das_instrument_box = da.DasInstrumentBox.factory()
        self.fiber_optical_path = da.FiberOpticalPath.factory()
        # Create core properties.
        self.core_properties = CoreProperties()
        self.core_properties_part = opc.package.Part(partname=PackURI('/docProps/core.xml'), content_type=CT.OPC_CORE_PROPERTIES, blob=None)
        self.package._add_relationship(reltype=RT.CORE_PROPERTIES, target=self.core_properties_part, rId='idCoreProperties')

    def save(self, validation_level=VALIDATION_LEVEL_XML_CONTENT):
        """Save the file.

        validation_level defines how much validation is done:
         * level 1 checks that XML data and package structure;
         * level 2 checks that external .h5 files are present;
         * level 3 checks contents of HDF files.
        """
        # Core properties.
        self.core_properties_part._blob = self.core_properties.make_xml()
        if validation_level >= VALIDATION_LEVEL_XML_VALIDITY:
            validation.validate_core_properties(self.core_properties_part._blob)
        # Das acquisition.
        if validation_level >= VALIDATION_LEVEL_XML_CONTENT:
            validation.validate_das_acquisition(self.das_acquisition)
        output = io.StringIO()
        output.write('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n')
        self.das_acquisition.export(output, 0,
                                namespacedef_ = 'xmlns:prodml="http://www.energistics.org/energyml/data/prodmlv2" xmlns:eml="http://www.energistics.org/energyml/data/commonv2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"')
        das_acquisition_xml = output.getvalue()
        output.close()
        if validation_level >= VALIDATION_LEVEL_XML_VALIDITY:
            validation.validate_xml(das_acquisition_xml, PRODML_SCHEMA.DAS_ACQUISITION)
        if self.das_acquisition_part:
            self.das_acquisition_part._blob = das_acquisition_xml
        else:
            self.das_acquisition_part = opc.package.Part(partname=PackURI('/DasAcquisition_'+self.das_acquisition.uuid+'.xml'), content_type=EPC_CT.DAS_ACQUISITION, blob=das_acquisition_xml)
            self.package._add_relationship(reltype=EPC_RT.DESTINATION_OBJECT, target=self.das_acquisition_part, rId='idAcquisition')
        # Das instrument box.
        if self.das_instrument_box:
            output = io.StringIO()
            output.write('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n')
            self.das_instrument_box.export(output, 0,
                                    namespacedef_ = 'xmlns:eml="http://www.energistics.org/energyml/data/commonv2" xmlns:prodml="http://www.energistics.org/energyml/data/prodmlv2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.energistics.org/energyml/data/prodmlv2 ../../../../xsd_schemas/DasInstrumentBox.xsd"')
            das_instrument_box_xml = output.getvalue()
            output.close()
            if validation_level >= VALIDATION_LEVEL_XML_VALIDITY:
                validation.validate_xml(das_instrument_box_xml, PRODML_SCHEMA.DAS_ACQUISITION)
            if self.das_instrument_box_part:
                self.das_instrument_box_part._blob = das_instrument_box_xml
            else:
                self.das_instrument_box_part = opc.package.Part(partname=PackURI('/DasInstrumentBox_'+self.das_instrument_box.uuid+'.xml'), content_type=EPC_CT.DAS_INSTRUMENT_BOX, blob=das_instrument_box_xml)
            if self.das_acquisition:
                self.das_acquisition_part._add_relationship(reltype=EPC_RT.DESTINATION_OBJECT, target=self.das_instrument_box_part, rId='_'+self.das_instrument_box.uuid)
                self.das_instrument_box_part._add_relationship(reltype=EPC_RT.SOURCE_OBJECT, target=self.das_acquisition_part, rId='_'+self.das_acquisition.uuid)
            else:
                self.package._add_relationship(reltype=EPC_RT.DESTINATION_OBJECT, target=das_instrument_box_part, rId='idDasInstrumentBox')
        else:
            raise ValueError('DasInstrumentBox is not present')
        # Fiber optical path.
        if self.fiber_optical_path:
            output = io.StringIO()
            output.write('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n')
            self.fiber_optical_path.export(output, 0,
                                    namespacedef_ = 'xmlns:eml="http://www.energistics.org/energyml/data/commonv2" xmlns:prodml="http://www.energistics.org/energyml/data/prodmlv2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.energistics.org/energyml/data/prodmlv2 ../../../../xsd_schemas/FiberOpticalPath.xsd"')

            fiber_optical_path_xml = output.getvalue()
            output.close()
            if validation_level >= VALIDATION_LEVEL_XML_VALIDITY:
                validation.validate_xml(fiber_optical_path_xml, PRODML_SCHEMA.FIBER_OPTICAL_PATH)
            if self.fiber_optical_path_part:
                self.fiber_optical_path_part._blob = fiber_optical_path_xml
            else:
                self.fiber_optical_path_part = opc.package.Part(partname=PackURI('/FiberOpticalPath_'+self.fiber_optical_path.uuid+'.xml'), content_type=EPC_CT.FIBER_OPTICAL_PATH, blob=fiber_optical_path_xml)
            if self.das_acquisition:
                self.das_acquisition_part._add_relationship(reltype=EPC_RT.DESTINATION_OBJECT, target=self.fiber_optical_path_part, rId='_'+self.fiber_optical_path.uuid)
                self.fiber_optical_path_part._add_relationship(reltype=EPC_RT.SOURCE_OBJECT, target=self.das_acquisition_part, rId='_'+self.das_acquisition.uuid)
            else:
                self.package._add_relationship(reltype=EPC_RT.DESTINATION_OBJECT, target=fiber_optical_path_part, rId='idFiberOpticalPath')
        else:
            raise ValueError('FiberOpticalPath is not present')

        # Update external files references and corresponding parts.
        self.update_epc_hdf_references(validation_level=validation_level)

        # Save the package.
        self.package.save(self.epc_path)

        if validation_level >= VALIDATION_LEVEL_EXTERNAL_CONTENT:
            validation.validate_external_objects(self)


    def update_epc_hdf_references(self, validation_level=VALIDATION_LEVEL_XML_VALIDITY):
        """Update epc so that every external file reference has an associated part.
        """
        external_hdf_files = {}
        for Raw in self.das_acquisition.Raw:
            for DasExternalDatasetPart in Raw.RawData.RawDataArray.Values.ExternalFileProxy:
                eepr = DasExternalDatasetPart.EpcExternalPartReference
                if eepr.Uuid not in self.eeprs:
                    external_hdf_files[eepr.Uuid] = str(eepr.Uuid) + '.h5'
            for DasExternalDatasetPart in Raw.RawDataTime.TimeArray.Values.ExternalFileProxy:
                eepr = DasExternalDatasetPart.EpcExternalPartReference
                if eepr.Uuid not in self.eeprs:
                    external_hdf_files[eepr.Uuid] = str(eepr.Uuid) + '.h5'
            if Raw.RawDataTriggerTime is not None:
                for DasExternalDatasetPart in Raw.RawDataTriggerTime.TimeArray.Values.ExternalFileProxy:
                    eepr = DasExternalDatasetPart.EpcExternalPartReference
                    if eepr.Uuid not in self.eeprs:
                        external_hdf_files[eepr.Uuid] = str(eepr.Uuid) + '.h5'
        if self.das_acquisition.Processed:
            for Fbe in self.das_acquisition.Processed.Fbe:
                for FbeData in Fbe.FbeData:
                    for DasExternalDatasetPart in FbeData.FbeDataArray.Values.ExternalFileProxy:
                        eepr = DasExternalDatasetPart.EpcExternalPartReference
                        if eepr.Uuid not in self.eeprs:
                            external_hdf_files[eepr.Uuid] = str(eepr.Uuid) + '.h5'
                for DasExternalDatasetPart in Fbe.FbeDataTime.TimeArray.Values.ExternalFileProxy:
                    eepr = DasExternalDatasetPart.EpcExternalPartReference
                    if eepr.Uuid not in self.eeprs:
                        external_hdf_files[eepr.Uuid] = str(eepr.Uuid) + '.h5'
            for Spectra in self.das_acquisition.Processed.Spectra:
                for DasExternalDatasetPart in Spectra.SpectraData.SpectraDataArray.Values.ExternalFileProxy:
                    eepr = DasExternalDatasetPart.EpcExternalPartReference
                    if eepr.Uuid not in self.eeprs:
                        external_hdf_files[eepr.Uuid] = str(eepr.Uuid) + '.h5'
                for DasExternalDatasetPart in Spectra.SpectraDataTime.TimeArray.Values.ExternalFileProxy:
                    eepr = DasExternalDatasetPart.EpcExternalPartReference
                    if eepr.Uuid not in self.eeprs:
                        external_hdf_files[eepr.Uuid] = str(eepr.Uuid) + '.h5'
        for eepr_uuid in external_hdf_files:
            if eepr_uuid in self.eeprs:
                continue
            eepr = da.EpcExternalPartReference.factory()
            eepr.schemaVersion = '2.0'
            eepr.uuid = eepr_uuid
            eepr.set_Citation(da.Citation.factory(Title='Hdf Proxy',
                                          Originator='Energistics',
                                          Creation=datetime.datetime.utcnow(),
                                          Format='Energistics'))
            output = io.StringIO()
            output.write('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n')
            eepr.export(output, 0,
                                    namespacedef_ = 'xmlns:eml="http://www.energistics.org/energyml/data/commonv2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.energistics.org/energyml/data/commonv2 ../../../common/v2.1/xsd_schemas/EmlAllObjects.xsd" xsi:type="eml:EpcExternalPartReference"')
            eepr_xml = output.getvalue()
            output.close()
            if validation_level >= VALIDATION_LEVEL_XML_VALIDITY:
                validation.validate_xml(eepr_xml, PRODML_SCHEMA.EPC_EXTERNAL_PART_REFERENCE)
            eepr_part = opc.package.Part(partname=PackURI('/EpcExternalPartReference_'+eepr_uuid+'.xml'), content_type=EPC_CT.EPC_EXTERNAL_PART_REFERENCE, blob=eepr_xml)
            self.das_acquisition_part._add_relationship(reltype=EPC_RT.EXTERNAL_PART_PROXY_TO_ML, target=eepr_part, rId='_'+eepr_uuid)
            eepr_part._add_relationship(reltype=EPC_RT.ML_TO_EXTERNAL_PART_PROXY, target=self.das_acquisition_part, rId='_'+self.das_acquisition.uuid)
            eepr_part._add_relationship(reltype=EPC_RT.EXTERNAL_RESOURCE, target=external_hdf_files[eepr_uuid], rId='Hdf5File', external=True)
            self.eeprs[eepr_uuid] = eepr

    def update_hdf_metadata(self, uuids=None):
        """Go through referenced HDF files, create them if needed, create datasets if needed (with minimal shape) and store required attributes from XML.

        if `uuids` is None, than HDF files corresponding to all objects are going to be updatedself.
        If `uuids` is not None, then only those Raw, Fbe and Spectra objects, whose uuid parameter is in `uuids`.  `uuids` in that case should be a collection.
        """
        for Raw in self.das_acquisition.Raw:
            if uuids is not None and Raw.uuid not in uuids:
                continue
            for DasExternalDatasetPart in Raw.RawData.RawDataArray.Values.ExternalFileProxy:
                eepr = DasExternalDatasetPart.EpcExternalPartReference
                filename = self.get_full_hdf_path(eepr.Uuid)
                self.update_hdf_rawdata_metadata(Raw, DasExternalDatasetPart, filename, eepr.Uuid)
            for DasExternalDatasetPart in Raw.RawDataTime.TimeArray.Values.ExternalFileProxy:
                eepr = DasExternalDatasetPart.EpcExternalPartReference
                filename = self.get_full_hdf_path(eepr.Uuid)
                self.update_hdf_rawtime_metadata(Raw, DasExternalDatasetPart, filename, eepr.Uuid)
            if Raw.RawDataTriggerTime is not None:
                for DasExternalDatasetPart in Raw.RawDataTriggerTime.TimeArray.Values.ExternalFileProxy:
                    eepr = DasExternalDatasetPart.EpcExternalPartReference
                    filename = self.get_full_hdf_path(eepr.Uuid)
                    self.update_hdf_rawtime_metadata(Raw, DasExternalDatasetPart, filename, eepr.Uuid)
        if self.das_acquisition.Processed:
            for Fbe in self.das_acquisition.Processed.Fbe:
                if uuids is not None and Fbe.uuid not in uuids:
                    continue
                for FbeData in Fbe.FbeData:
                    for DasExternalDatasetPart in FbeData.FbeDataArray.Values.ExternalFileProxy:
                        eepr = DasExternalDatasetPart.EpcExternalPartReference
                        filename = self.get_full_hdf_path(eepr.Uuid)
                        self.update_hdf_fbedata_metadata(Fbe, FbeData, DasExternalDatasetPart, filename, eepr.Uuid)
                for DasExternalDatasetPart in Fbe.FbeDataTime.TimeArray.Values.ExternalFileProxy:
                    eepr = DasExternalDatasetPart.EpcExternalPartReference
                    filename = self.get_full_hdf_path(eepr.Uuid)
                    self.update_hdf_fbetime_metadata(Fbe, DasExternalDatasetPart, filename, eepr.Uuid)
            for Spectra in self.das_acquisition.Processed.Spectra:
                if uuids is not None and Spectra.uuid not in uuids:
                    continue
                for DasExternalDatasetPart in Spectra.SpectraData.SpectraDataArray.Values.ExternalFileProxy:
                    eepr = DasExternalDatasetPart.EpcExternalPartReference
                    filename = self.get_full_hdf_path(eepr.Uuid)
                    self.update_hdf_spectradata_metadata(Spectra, DasExternalDatasetPart, filename, eepr.Uuid)
                for DasExternalDatasetPart in Spectra.SpectraDataTime.TimeArray.Values.ExternalFileProxy:
                    eepr = DasExternalDatasetPart.EpcExternalPartReference
                    filename = self.get_full_hdf_path(eepr.Uuid)
                    self.update_hdf_spectratime_metadata(Spectra, DasExternalDatasetPart, filename, eepr.Uuid)

    def update_hdf_rawdata_metadata(self, Raw, DasExternalDatasetPart, filename, uuid):
        """Update metadata from DasRawData to HDF file.
        """
        self.update_hdf_acquisition_metadata(filename, uuid)
        self.update_hdf_raw_metadata(Raw, filename, parent_hdf_path(DasExternalDatasetPart.PathInExternalFile))
        with h5py.File(filename, 'a') as hdf_file:
            dataset_path = DasExternalDatasetPart.PathInExternalFile
            if dataset_path in hdf_file:
                DasExternalDatasetPart.Count = hdf_file[dataset_path].size
            else:
                shape = [None, None]
                shape[Raw.RawData.Dimensions.index('time')] = DasExternalDatasetPart.Count // Raw.NumberOfLoci
                shape[Raw.RawData.Dimensions.index('locus')] = Raw.NumberOfLoci
                hdf_file.require_dataset(dataset_path,
                                         shape=shape,
                                         maxshape=(None, None),
                                         chunks=True,
                                         dtype=self.raw_dtype)
            hdf_file[dataset_path].attrs['Count'] = DasExternalDatasetPart.Count
            hdf_file[dataset_path].attrs['Dimensions'] = [n.encode("ascii", "ignore") for n in Raw.RawData.Dimensions]
            hdf_file[dataset_path].attrs['PartEndTime'] = DasExternalDatasetPart.PartEndTime
            hdf_file[dataset_path].attrs['PartStartTime'] = DasExternalDatasetPart.PartStartTime
            hdf_file[dataset_path].attrs['StartIndex'] = DasExternalDatasetPart.StartIndex

    def update_hdf_rawtime_metadata(self, Raw, DasExternalDatasetPart, filename, uuid):
        """Write metadata from DasRawDataTime to HDF file.
        """
        self.update_hdf_acquisition_metadata(filename, uuid)
        self.update_hdf_raw_metadata(Raw, filename, parent_hdf_path(DasExternalDatasetPart.PathInExternalFile))
        with h5py.File(filename, 'a') as hdf_file:
            dataset_path = DasExternalDatasetPart.PathInExternalFile
            if dataset_path in hdf_file:
                DasExternalDatasetPart.Count = hdf_file[dataset_path].size
            else:
                hdf_file.require_dataset(dataset_path,
                                         shape=(DasExternalDatasetPart.Count,),
                                         maxshape=(None,),
                                         chunks=True,
                                         dtype=self.time_dtype)
            hdf_file[dataset_path].attrs['Count'] = DasExternalDatasetPart.Count
            hdf_file[dataset_path].attrs['PartEndTime'] = DasExternalDatasetPart.PartEndTime
            hdf_file[dataset_path].attrs['PartStartTime'] = DasExternalDatasetPart.PartStartTime
            hdf_file[dataset_path].attrs['StartIndex'] = DasExternalDatasetPart.StartIndex
            hdf_file[dataset_path].attrs['EndTime'] = Raw.RawDataTime.EndTime
            hdf_file[dataset_path].attrs['StartTime'] = Raw.RawDataTime.StartTime

    def update_hdf_fbedata_metadata(self, Fbe, FbeData, DasExternalDatasetPart, filename, uuid):
        """Write metadata from DasFbeData to HDF file.
        """
        self.update_hdf_acquisition_metadata(filename, uuid)
        self.update_hdf_fbe_metadata(Fbe, filename, parent_hdf_path(DasExternalDatasetPart.PathInExternalFile))
        with h5py.File(filename, 'a') as hdf_file:
            dataset_path = DasExternalDatasetPart.PathInExternalFile
            if dataset_path in hdf_file:
                DasExternalDatasetPart.Count = hdf_file[dataset_path].size
            else:
                shape = [None, None]
                shape[FbeData.Dimensions.index('time')] = DasExternalDatasetPart.Count // Fbe.NumberOfLoci
                shape[FbeData.Dimensions.index('locus')] = Fbe.NumberOfLoci
                hdf_file.require_dataset(dataset_path,
                                         shape=shape,
                                         maxshape=(None, None),
                                         chunks=True,
                                         dtype=self.fbe_dtype)
            hdf_file[dataset_path].attrs['Count'] = DasExternalDatasetPart.Count
            hdf_file[dataset_path].attrs['Dimensions'] = [n.encode("ascii", "ignore") for n in FbeData.Dimensions]
            hdf_file[dataset_path].attrs['EndFrequency'] = float(FbeData.EndFrequency.valueOf_)
            hdf_file[dataset_path].attrs['PartStartTime'] = DasExternalDatasetPart.PartStartTime
            hdf_file[dataset_path].attrs['PartEndTime'] = DasExternalDatasetPart.PartEndTime
            hdf_file[dataset_path].attrs['StartFrequency'] = float(FbeData.StartFrequency.valueOf_)
            hdf_file[dataset_path].attrs['StartIndex'] = DasExternalDatasetPart.StartIndex

    def update_hdf_fbetime_metadata(self, Fbe, DasExternalDatasetPart, filename, uuid):
        """Write metadata from DasFbeDataTime to HDF file.
        """
        self.update_hdf_acquisition_metadata(filename, uuid)
        self.update_hdf_fbe_metadata(Fbe, filename, parent_hdf_path(DasExternalDatasetPart.PathInExternalFile))
        with h5py.File(filename, 'a') as hdf_file:
            dataset_path = DasExternalDatasetPart.PathInExternalFile
            if dataset_path in hdf_file:
                DasExternalDatasetPart.Count = hdf_file[dataset_path].size
            else:
                hdf_file.require_dataset(dataset_path,
                                         shape=(DasExternalDatasetPart.Count,),
                                         maxshape=(None,),
                                         chunks=True,
                                         dtype=self.time_dtype)
            hdf_file[dataset_path].attrs['Count'] = DasExternalDatasetPart.Count
            hdf_file[dataset_path].attrs['PartEndTime'] = DasExternalDatasetPart.PartEndTime
            hdf_file[dataset_path].attrs['PartStartTime'] = DasExternalDatasetPart.PartStartTime
            hdf_file[dataset_path].attrs['StartIndex'] = DasExternalDatasetPart.StartIndex
            hdf_file[dataset_path].attrs['EndTime'] = Fbe.FbeDataTime.EndTime
            hdf_file[dataset_path].attrs['StartTime'] = Fbe.FbeDataTime.StartTime

    def update_hdf_spectradata_metadata(self, Spectra, DasExternalDatasetPart, filename, uuid):
        """Write metadata from DasSpectraData to HDF file.
        """
        self.update_hdf_acquisition_metadata(filename, uuid)
        self.update_hdf_spectra_metadata(Spectra, filename, parent_hdf_path(DasExternalDatasetPart.PathInExternalFile))
        with h5py.File(filename, 'a') as hdf_file:
            dataset_path = DasExternalDatasetPart.PathInExternalFile
            if dataset_path in hdf_file:
                DasExternalDatasetPart.Count = hdf_file[dataset_path].size
            else:
                shape = [None, None, None]
                shape[Spectra.SpectraData.Dimensions.index('time')] = DasExternalDatasetPart.Count // Spectra.NumberOfLoci // Spectra.TransformSize
                shape[Spectra.SpectraData.Dimensions.index('locus')] = Spectra.NumberOfLoci
                shape[Spectra.SpectraData.Dimensions.index('frequency')] = Spectra.TransformSize
                hdf_file.require_dataset(dataset_path,
                                         shape=shape,
                                         maxshape=(None, None, None),
                                         chunks=True,
                                         dtype=self.spectra_dtype)
            hdf_file[dataset_path].attrs['Count'] = DasExternalDatasetPart.Count
            hdf_file[dataset_path].attrs['Dimensions'] = [n.encode("ascii", "ignore") for n in Spectra.SpectraData.Dimensions]
            hdf_file[dataset_path].attrs['EndFrequency'] = float(Spectra.SpectraData.EndFrequency.valueOf_)
            hdf_file[dataset_path].attrs['PartStartTime'] = DasExternalDatasetPart.PartStartTime
            hdf_file[dataset_path].attrs['PartEndTime'] = DasExternalDatasetPart.PartEndTime
            hdf_file[dataset_path].attrs['StartFrequency'] = float(Spectra.SpectraData.StartFrequency.valueOf_)
            hdf_file[dataset_path].attrs['StartIndex'] = DasExternalDatasetPart.StartIndex

    def update_hdf_spectratime_metadata(self, Spectra, DasExternalDatasetPart, filename, uuid):
        """Write metadata from DasFbeDataTime to HDF file.
        """
        self.update_hdf_acquisition_metadata(filename, uuid)
        self.update_hdf_spectra_metadata(Spectra, filename, parent_hdf_path(DasExternalDatasetPart.PathInExternalFile))
        with h5py.File(filename, 'a') as hdf_file:
            dataset_path = DasExternalDatasetPart.PathInExternalFile
            if dataset_path in hdf_file:
                DasExternalDatasetPart.Count = hdf_file[dataset_path].size
            else:
                hdf_file.require_dataset(dataset_path,
                                         shape=(DasExternalDatasetPart.Count,),
                                         maxshape=(None,),
                                         chunks=True,
                                         dtype=self.time_dtype)
            hdf_file[dataset_path].attrs['Count'] = DasExternalDatasetPart.Count
            hdf_file[dataset_path].attrs['PartEndTime'] = DasExternalDatasetPart.PartEndTime
            hdf_file[dataset_path].attrs['PartStartTime'] = DasExternalDatasetPart.PartStartTime
            hdf_file[dataset_path].attrs['StartIndex'] = DasExternalDatasetPart.StartIndex
            hdf_file[dataset_path].attrs['EndTime'] = Spectra.SpectraDataTime.EndTime
            hdf_file[dataset_path].attrs['StartTime'] = Spectra.SpectraDataTime.StartTime

    def update_hdf_acquisition_metadata(self, filename, uuid):
        """Write hdf metadata.
        """
        # Create das hdf file.
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with h5py.File(filename, 'a') as hdf_file:
            hdf_file.attrs['uuid'] = uuid
            # Acquisition group.
            group_acquisition = hdf_file.require_group('Acquisition')
            if self.das_acquisition.AcquisitionDescription is not None:
                group_acquisition.attrs['AcquisitionDescription'] = self.das_acquisition.AcquisitionDescription
            group_acquisition.attrs['AcquisitionId'] = self.das_acquisition.AcquisitionId
            group_acquisition.attrs['FacilityId'] = [n.encode("ascii", "ignore") for n in self.das_acquisition.FacilityId]
            group_acquisition.attrs['GaugeLength'] = float(self.das_acquisition.GaugeLength.valueOf_)
            if self.das_acquisition.GaugeLengthUnit is not None:
                group_acquisition.attrs['GaugeLengthUnit'] = self.das_acquisition.GaugeLengthUnit
            else:
                group_acquisition.attrs['GaugeLengthUnit'] = self.das_acquisition.GaugeLength.uom
            group_acquisition.attrs['MaximumFrequency'] = float(self.das_acquisition.MaximumFrequency.valueOf_)
            group_acquisition.attrs['MeasurementStartTime'] = self.das_acquisition.MeasurementStartTime
            group_acquisition.attrs['MinimumFrequency'] = float(self.das_acquisition.MinimumFrequency.valueOf_)
            group_acquisition.attrs['NumberOfLoci'] = self.das_acquisition.NumberOfLoci
            group_acquisition.attrs['PulseRate'] = float(self.das_acquisition.PulseRate.valueOf_)
            group_acquisition.attrs['PulseWidth'] = float(self.das_acquisition.PulseWidth.valueOf_)
            if self.das_acquisition.PulseWidthUnit is not None:
                group_acquisition.attrs['PulseWidthUnit'] = self.das_acquisition.PulseWidthUnit
            else:
                group_acquisition.attrs['PulseWidthUnit'] = self.das_acquisition.PulseWidth.uom
            group_acquisition.attrs['schemaVersion'] = self.das_acquisition.schemaVersion
            group_acquisition.attrs['SpatialSamplingInterval'] = float(self.das_acquisition.SpatialSamplingInterval.valueOf_)
            if self.das_acquisition.SpatialSamplingIntervalUnit is not None:
                group_acquisition.attrs['SpatialSamplingIntervalUnit'] = self.das_acquisition.SpatialSamplingIntervalUnit
            else:
                group_acquisition.attrs['SpatialSamplingIntervalUnit'] = self.das_acquisition.SpatialSamplingInterval.uom
            group_acquisition.attrs['StartLocusIndex'] = self.das_acquisition.StartLocusIndex
            group_acquisition.attrs['TriggeredMeasurement'] = self.das_acquisition.TriggeredMeasurement
            group_acquisition.attrs['uuid'] = self.das_acquisition.uuid
            group_acquisition.attrs['VendorCode'] = self.das_acquisition.VendorCode.Name
            # Calibration
            index = 0
            calibration_dtype = np.dtype([('CalibrationLocusIndex', np.uint64),
                                          ('CalibrationOpticalPathDistance', np.float64),
                                          ('CalibrationFacilityLength', np.float64),
                                          ('CalibrationType', 'S64')])
            for calibration in self.das_acquisition.Calibration:
                group_name = 'Acquisition/Calibration['+str(index)+']'
                group_calibration = hdf_file.require_group(group_name)
                if calibration.CalibrationDatum is not None:
                    group_calibration.attrs['CalibrationDatum'] = calibration.CalibrationDatum
                if calibration.CalibrationDescription is not None:
                    group_calibration.attrs['CalibrationDescription'] = calibration.CalibrationDescription
                if calibration.CalibrationFacilityLengthUnit is not None:
                    group_calibration.attrs['CalibrationFacilityLengthUnit'] = calibration.CalibrationFacilityLengthUnit
                if calibration.CalibrationIndex is not None:
                    group_calibration.attrs['CalibrationIndex'] = calibration.CalibrationIndex
                if calibration.CalibrationOpticalPathDistanceUnit is not None:
                    group_calibration.attrs['CalibrationOpticalPathDistanceUnit'] = calibration.CalibrationOpticalPathDistanceUnit
                group_calibration.attrs['FacilityKind'] = calibration.FacilityKind
                group_calibration.attrs['FacilityName'] = calibration.FacilityName
                group_calibration.attrs['NumberOfCalibrationPoints'] = calibration.NumberOfCalibrationPoints
                dset_name = group_name+'/CalibrationDataPoints'
                hdf_file.require_dataset(dset_name,
                                        shape=(calibration.NumberOfCalibrationPoints,),
                                        dtype=calibration_dtype)
                for j, cdp in enumerate(calibration.CalibrationDataPoints):
                    hdf_file[dset_name][j, 'CalibrationLocusIndex'] = cdp.CalibrationLocusIndex
                    hdf_file[dset_name][j, 'CalibrationOpticalPathDistance'] = cdp.CalibrationOpticalPathDistance.valueOf_
                    hdf_file[dset_name][j, 'CalibrationFacilityLength'] = cdp.CalibrationFacilityLength.valueOf_
                    hdf_file[dset_name][j, 'CalibrationType'] = cdp.CalibrationType
                index += 1
            hdf_file.require_group('Acquisition/Custom')

    def update_hdf_raw_metadata(self, Raw, filename, group_path):
        """Write Raw metadata."""
        with h5py.File(filename, 'a') as hdf_file:
            group_raw = hdf_file.require_group(group_path)
            group_raw.attrs['NumberOfLoci'] = Raw.NumberOfLoci
            group_raw.attrs['RawDataUnit'] = Raw.RawDataUnit
            group_raw.attrs['StartLocusIndex'] = Raw.StartLocusIndex
            group_raw.attrs['uuid'] = Raw.uuid
            if Raw.OutputDataRate is not None:
                group_raw.attrs['OutputDataRate'] = float(Raw.OutputDataRate.valueOf_)
            if Raw.RawDescription is not None:
                group_raw.attrs['RawDescription'] = Raw.RawDescription
            if Raw.RawIndex is not None:
                group_raw.attrs['RawIndex'] = Raw.RawIndex

    def update_hdf_fbe_metadata(self, Fbe, filename, group_path):
        with h5py.File(filename, 'a') as hdf_file:
            hdf_file.require_group(group_path)
            hdf_file[group_path].attrs['FbeDataUnit'] = Fbe.FbeDataUnit
            hdf_file[group_path].attrs['OutputDataRate'] = float(Fbe.OutputDataRate.valueOf_)
            hdf_file[group_path].attrs['StartLocusIndex'] = Fbe.StartLocusIndex
            hdf_file[group_path].attrs['NumberOfLoci'] = Fbe.NumberOfLoci
            hdf_file[group_path].attrs['uuid'] = Fbe.uuid
            if Fbe.FbeIndex is not None:
                hdf_file[group_path].attrs['FbeIndex'] = Fbe.FbeIndex
            if Fbe.FbeDescription is not None:
                hdf_file[group_path].attrs['FbeDescription'] = Fbe.FbeDescription
            if Fbe.FilterType is not None:
                hdf_file[group_path].attrs['FilterType'] = Fbe.FilterType
            if Fbe.SpatialSamplingInterval is not None:
                hdf_file[group_path].attrs['SpatialSamplingInterval'] = float(Fbe.SpatialSamplingInterval.valueOf_)
            if Fbe.SpatialSamplingIntervalUnit is not None:
                hdf_file[group_path].attrs['SpatialSamplingIntervalUnit'] = Fbe.SpatialSamplingIntervalUnit
            if Fbe.SpectraReference is not None:
                hdf_file[group_path].attrs['SpectraReference'] = Fbe.SpectraReference
            if Fbe.TransformType is not None:
                hdf_file[group_path].attrs['TransformType'] = Fbe.TransformType
            if Fbe.TransformSize is not None:
                hdf_file[group_path].attrs['TransformSize'] = Fbe.TransformSize
            if Fbe.RawReference is not None:
                hdf_file[group_path].attrs['RawReference'] = Fbe.RawReference
            if Fbe.WindowFunction is not None:
                hdf_file[group_path].attrs['WindowFunction'] = Fbe.WindowFunction
            if Fbe.WindowSize is not None:
                hdf_file[group_path].attrs['WindowSize'] = Fbe.WindowSize
            if Fbe.WindowOverlap is not None:
                hdf_file[group_path].attrs['WindowOverlap'] = Fbe.WindowOverlap

    def update_hdf_spectra_metadata(self, Spectra, filename, group_path):
        """Write spectra metadata."""
        with h5py.File(filename, 'a') as hdf_file:
            hdf_file.require_group(group_path)
            hdf_file[group_path].attrs['SpectraDataUnit'] = Spectra.SpectraDataUnit
            hdf_file[group_path].attrs['OutputDataRate'] = float(Spectra.OutputDataRate.valueOf_)
            hdf_file[group_path].attrs['StartLocusIndex'] = Spectra.StartLocusIndex
            hdf_file[group_path].attrs['NumberOfLoci'] = Spectra.NumberOfLoci
            hdf_file[group_path].attrs['uuid'] = Spectra.uuid
            if Spectra.SpectraIndex is not None:
                hdf_file[group_path].attrs['SpectraIndex'] = Spectra.SpectraIndex
            if Spectra.SpectraDescription is not None:
                hdf_file[group_path].attrs['SpectraDescription'] = Spectra.SpectraDescription
            if Spectra.FilterType is not None:
                hdf_file[group_path].attrs['FilterType'] = Spectra.FilterType
            if Spectra.SpatialSamplingInterval is not None:
                hdf_file[group_path].attrs['SpatialSamplingInterval'] = float(Spectra.SpatialSamplingInterval.valueOf_)
            if Spectra.SpatialSamplingIntervalUnit is not None:
                hdf_file[group_path].attrs['SpatialSamplingIntervalUnit'] = Spectra.SpatialSamplingIntervalUnit
            if Spectra.FbeReference is not None:
                hdf_file[group_path].attrs['FbeReference'] = Spectra.FbeReference
            if Spectra.RawReference is not None:
                hdf_file[group_path].attrs['RawReference'] = Spectra.RawReference
            if Spectra.TransformType is not None:
                hdf_file[group_path].attrs['TransformType'] = Spectra.TransformType
            if Spectra.TransformSize is not None:
                hdf_file[group_path].attrs['TransformSize'] = Spectra.TransformSize
            if Spectra.WindowFunction is not None:
                hdf_file[group_path].attrs['WindowFunction'] = Spectra.WindowFunction
            if Spectra.WindowSize is not None:
                hdf_file[group_path].attrs['WindowSize'] = Spectra.WindowSize
            if Spectra.WindowOverlap is not None:
                hdf_file[group_path].attrs['WindowOverlap'] = Spectra.WindowOverlap

    def get_hdf_filename(self, uuid):
        """Return filename of the external hdf file by its reference uuid (stored in DataObjectReference.Uuid).
        """
        self.update_epc_hdf_references()
        for rel in self.das_acquisition_part.rels:
            if rel._reltype == EPC_RT.EXTERNAL_PART_PROXY_TO_ML:
                for eepr_rel in rel.target_part.rels:
                    eepr = load_eepr(rel.target_part.blob)
                    if eepr_rel._reltype == EPC_RT.EXTERNAL_RESOURCE:
                        if eepr.uuid == uuid:
                            return eepr_rel.target_ref
        raise KeyError('No filename for {} found'.format(uuid))

    def set_hdf_filename(self, uuid, new_filename):
        """Change filename of the external hdf corresponding to its reference uuid (stored in DataObjectReference.Uuid).

        Should be called before creating an hdf file.
        """
        self.update_epc_hdf_references()
        for rel in self.das_acquisition_part.rels:
            if rel._reltype == EPC_RT.EXTERNAL_PART_PROXY_TO_ML:
                for eepr_rel in rel.target_part.rels:
                    eepr = load_eepr(rel.target_part.blob)
                    if eepr_rel._reltype == EPC_RT.EXTERNAL_RESOURCE:
                        if eepr.uuid == uuid:
                            eepr_rel._target = new_filename
                            return
        raise KeyError('No filename for {} found'.format(uuid))

    def get_full_hdf_path(self, uuid):
        """Return full path to the external hdf file by its reference uuid (stored in DataObjectReference.Uuid).
        """
        return os.path.join(self.epc_folder, self.get_hdf_filename(uuid))

    def get_object_by_uuid(self, uuid):
        """Return a subobject of DasAcquisition by its uuid, or None otherwise.

        Only checks the following types of objects:  Raw, Fbe, Spectra.
        """
        if isinstance(uuid, bytes):
            uuid = uuid.decode()
        for Raw in self.das_acquisition.Raw:
            if Raw.uuid == uuid:
                return Raw
        if self.das_acquisition.Processed is not None:
            for Fbe in self.das_acquisition.Processed.Fbe:
                if Fbe.uuid == uuid:
                    return Fbe
            for Spectra in self.das_acquisition.Processed.Spectra:
                if Spectra.uuid == uuid:
                    return Spectra
        return None
