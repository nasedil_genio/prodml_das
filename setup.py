# License notice
#
# Copyright 2017
#
# Energistics
# The following Energistics (c) products were used in the creation of this work:
#
# •             PRODML Data Schema Specifications, Version 1.3.1
# •             PRODML Data Schema Specifications, Version 2.0
#
# All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
# any portion thereof, which remain in the Standards DevKit shall remain with Energistics
# or its suppliers and shall remain subject to the terms of the Product License Agreement
# available at http://www.energistics.org/product-license-agreement.
#
# Apache
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
# except in compliance with the License.
#
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the
# License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.
#
# See the License for the specific language governing permissions and limitations under the
# License.
#
# All rights reserved.
#

import os

from setuptools import setup

from subprocess import check_call

# In the following lines we calculate and use relative path, because generateDS fails with absolute path (probably due to circular include handling).
setup_folder = os.path.abspath(os.path.dirname(__file__))
generate_classes = ('generateDS',
                    '-o', os.path.relpath(os.path.join(setup_folder, 'prodml_das', 'DasAcquisition.py'), os.getcwd()),
                    '-f',
                    '-m',
                    '--export=write literal',
                    os.path.relpath(os.path.join(setup_folder, 'energistics', 'prodml', 'v2.0', 'xsd_schemas', 'DasAcquisition.xsd'), os.getcwd()))
check_call(generate_classes)

from prodml_das import __version__

setup(name='prodml_das',
      version=__version__,
      description="Read/write DAS data from PRODML files",
      long_description=(
          'This is a Python 3.5+ package to read and write PRODML '
          'files, used for Distributed Accoustic Sensing (DAS) data. '
          'It\'s an implementation of the energistics standard PRODML v2.0 '
          'which may be found here: http://www.energistics.org/production/prodml-standards/current-standards '
          'The standard is very new and can be complex to get up and running, so this library is meant to '
          'be a help to be able to quickly read and write PRODML data and understand how the standard may be '
          'used in a practical setting.\n\n'),
      author="Energistics",
      license="MIT",
      classifiers=[
          "Development Status :: 2 - Pre-Alpha",
          "Environment :: Console",
          "Environment :: Web Environment",
          "Environment :: Other Environment",
          "Intended Audience :: Customer Service",
          "Intended Audience :: Developers",
          "Intended Audience :: Education",
          "Intended Audience :: End Users/Desktop",
          "Intended Audience :: Other Audience",
          "Intended Audience :: Science/Research",
          "License :: OSI Approved :: MIT License",
          "Natural Language :: English",
          "Operating System :: OS Independent",
          "Programming Language :: Python :: 3.5",
          "Programming Language :: Python :: 3.6",
          "Topic :: Scientific/Engineering",
          "Topic :: System :: Filesystems",
          "Topic :: Scientific/Engineering :: Information Analysis",
      ],
      keywords="science geophysics io",
      packages=["prodml_das", ],
      package_data={'prodml_das': ['../energistics/common/v2.1/xsd_schemas/*.xsd',
                                   '../energistics/common/v2.1/xsd_schemas/gml/*/*.xsd',
                                   '../energistics/common/v2.1/xsd_schemas/iso/*/*/*.xsd',
                                   '../energistics/common/v2.1/xsd_schemas/iso/*/*/*/*.xsd',
                                   '../energistics/common/v2.1/xsd_schemas/xlink/*/*.xsd',
                                   '../energistics/prodml/v2.0/xsd_schemas/*.xsd']},
      entry_points={
          'console_scripts': [
          ],
      },
      include_package_data=True,
      install_requires=[
          'aniso8601',
          'h5py',
          'lxml',
          'python-opc'
      ],
      setup_requires=[
          'generateds==2.28.2'
      ],
      tests_require=[
          'pytest'
      ]
)
